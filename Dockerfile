FROM node:8-alpine
LABEL maintainer Ross Affandy <ross.affandy@imoney-group.com>

ARG ssh_private_key
ARG build_region
ARG env
ARG lang
ARG country
ENV build_region=${build_region}
ENV env=${env}
ENV lang=${lang}
ENV country=${country}
ENV NR_NATIVE_METRICS_NO_BUILD=true

RUN mkdir /root/.ssh/ && \
    chmod 0700 /root/.ssh && \
    apk add --no-cache --virtual .builddeps openssh-client git build-base make gcc g++ python && \
    ssh-keyscan bitbucket.org > /root/.ssh/known_hosts && \
    echo "${ssh_private_key}" > /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa

WORKDIR /app

COPY . /app

#RUN cp /app/config/app.json.sample /app/config/app.json

RUN if [ "$env" = "development" ] ; then cp /app/config/development.json.sample /app/config/development.json ; else cp /app/config/production.json.sample /app/config/production.json ; fi

RUN if [ "$env" = "development" ] ; then npm install --unsafe-perm ; else npm install --production --unsafe-perm ; fi

RUN npm audit fix && \
    rm /root/.ssh/id_rsa && \
    apk del .builddeps

# FIXME: The following ask for public key for bitbucket
# RUN install-capi-client.sh

EXPOSE 3000

ENTRYPOINT ["/bin/sh"]

#start:production:id:id

CMD ["-c" , "npm run start:${env}:${country}:${lang}"]

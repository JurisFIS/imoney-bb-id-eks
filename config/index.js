const region = process.env.REGION || 'my';

const lang = process.env.LANGUAGE || 'en';
const env = process.env.NODE_ENV || global['app'].get('env') || 'production';

let configJSON;

try {
  configJSON = require(`./${env}.json`);
} catch (e) {
  throw new ReferenceError(`${env}.json file not found`);
}

if (!configJSON[`${region}-${lang}`]) throw new ReferenceError(
    `Configuration for '${region}-${lang}' not found in ${env}.json`);
const config = Object.assign({}, configJSON.default,
    configJSON[`${region}-${lang}`],
    {region, lang});

// Debugging
const debug = require('debug')(`${config.name}:config`);
debug(`Current configuration is set to ${region}-${lang}`);

// Get a custom configuration
config.getCustom = (kwargs = {}) => {
  const region = kwargs.region || 'my';
  const lang = kwargs.lang || 'en';
  return Object.assign(
      configJSON.default, kwargs, configJSON[`${region}-${lang}`]);
};

// Exporting
module.exports = config;

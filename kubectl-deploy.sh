#!/usr/bin/env bash

appname=$1
region=$2
tag=$3
git_hash=$4
env=$5

if [ -z ${appname} ]; then
    echo "No appname provided"
    exit 1
fi

if [ -z ${region} ]; then
    echo "No region provided"
    exit 1
fi

if [ -z ${tag} ]; then
    echo "No tag provided"
    exit 1
fi

if [ -z ${git_hash} ]; then
    echo "No git hash provided"
    exit 1
fi

if [ -z ${env} ]; then
    echo "No environment provided"
    exit 1
fi

valid_regions=(my ms id ph sg)

for i in "${valid_regions[@]}"
do
    if [ "$i" == ${region} ] ; then
        cat deployment_"${env}".yaml | sed -e "s/<appname>/${appname}/g" | sed -e "s/<region>/${region}/g" | sed -e "s/<tag>/${tag}/g" | sed -e "s/<appname>-<region>/${appname}-${region}/g" | sed -e "s/<git_hash>/${git_hash}/g" | kubectl apply -f -
    fi
done

// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
// jQuery RSS
(function(d){var e=function(a,b,c,f){this.target=a;this.url=b;this.html=[];this.effectQueue=[];this.options=d.extend({ssl:!1,host:"www.feedrapp.info",limit:null,key:null,layoutTemplate:"<ul>{entries}</ul>",entryTemplate:'<li><a href="{url}">[{author}@{date}] {title}</a><br/>{shortBodyPlain}</li>',tokens:{},outputMode:"json",dateFormat:"dddd MMM Do",dateLocale:"en",effect:"show",offsetStart:!1,offsetEnd:!1,error:function(){console.log("jQuery RSS: url doesn't link to RSS-Feed")},onData:function(){},
success:function(){}},c||{});this.callback=f||this.options.success};e.htmlTags="doctype,html,head,title,base,link,meta,style,script,noscript,body,article,nav,aside,section,header,footer,h1-h6,hgroup,address,p,hr,pre,blockquote,ol,ul,li,dl,dt,dd,figure,figcaption,div,table,caption,thead,tbody,tfoot,tr,th,td,col,colgroup,form,fieldset,legend,label,input,button,select,datalist,optgroup,option,textarea,keygen,output,progress,meter,details,summary,command,menu,del,ins,img,iframe,embed,object,param,video,audio,source,canvas,track,map,area,a,em,strong,i,b,u,s,small,abbr,q,cite,dfn,sub,sup,time,code,kbd,samp,var,mark,bdi,bdo,ruby,rt,rp,span,br,wbr".split(",");
e.prototype.load=function(a){var b="http"+(this.options.ssl?"s":"")+"://"+this.options.host+"?callback=?&q="+encodeURIComponent(this.url);this.options.offsetStart&&this.options.offsetEnd&&(this.options.limit=this.options.offsetEnd);null!==this.options.limit&&(b+="&num="+this.options.limit);null!==this.options.key&&(b+="&key="+this.options.key);d.getJSON(b,a)};e.prototype.render=function(){var a=this;this.load(function(b){try{a.feed=b.responseData.feed,a.entries=b.responseData.feed.entries}catch(c){return a.entries=
[],a.feed=null,a.options.error.call(a)}b=a.generateHTMLForEntries();a.target.append(b.layout);if(0!==b.entries.length){d.isFunction(a.options.onData)&&a.options.onData.call(a);var f=d(b.layout).is("entries")?b.layout:d("entries",b.layout);a.appendEntriesAndApplyEffects(f,b.entries)}0<a.effectQueue.length?a.executeEffectQueue(a.callback):d.isFunction(a.callback)&&a.callback.call(a)})};e.prototype.appendEntriesAndApplyEffects=function(a,b){var c=this;d.each(b,function(b,e){var d=c.wrapContent(e);"show"===
c.options.effect?a.before(d):(d.css({display:"none"}),a.before(d),c.applyEffect(d,c.options.effect))});a.remove()};e.prototype.generateHTMLForEntries=function(){var a=this,b={entries:[],layout:null};d(this.entries).each(function(){var c=a.options.offsetStart,f=a.options.offsetEnd;c&&f?index>=c&&index<=f&&a.isRelevant(this,b.entries)&&(c=a.evaluateStringForEntry(a.options.entryTemplate,this),b.entries.push(c)):a.isRelevant(this,b.entries)&&(c=a.evaluateStringForEntry(a.options.entryTemplate,this),
b.entries.push(c))});b.layout=this.options.entryTemplate?this.wrapContent(this.options.layoutTemplate.replace("{entries}","<entries></entries>")):this.wrapContent("<div><entries></entries></div>");return b};e.prototype.wrapContent=function(a){return 0!==d.trim(a).indexOf("<")?d("<div>"+a+"</div>"):d(a)};e.prototype.applyEffect=function(a,b,c){switch(b){case "slide":a.slideDown("slow",c);break;case "slideFast":a.slideDown(c);break;case "slideSynced":this.effectQueue.push({element:a,effect:"slide"});
break;case "slideFastSynced":this.effectQueue.push({element:a,effect:"slideFast"})}};e.prototype.executeEffectQueue=function(a){var b=this;this.effectQueue.reverse();var c=function(){var f=b.effectQueue.pop();f?b.applyEffect(f.element,f.effect,c):a&&a()};c()};e.prototype.evaluateStringForEntry=function(a,b){var c=a,f=this;d(a.match(/(\{.*?\})/g)).each(function(){var a=this.toString();c=c.replace(a,f.getValueForToken(a,b))});return c};e.prototype.isRelevant=function(a,b){var c=this.getTokenMap(a);
return this.options.filter?this.options.filterLimit&&this.options.filterLimit===b.length?!1:this.options.filter(a,c):!0};e.prototype.getFormattedDate=function(a){if(this.options.dateFormatFunction)return this.options.dateFormatFunction(a);return"undefined"!==typeof moment?(a=moment(new Date(a)),a=a.locale?a.locale(this.options.dateLocale):a.lang(this.options.dateLocale),a.format(this.options.dateFormat)):a};e.prototype.getTokenMap=function(a){if(!this.feedTokens){var b=JSON.parse(JSON.stringify(this.feed));
delete b.entries;this.feedTokens=b}return d.extend({feed:this.feedTokens,url:a.link,author:a.author,date:this.getFormattedDate(a.publishedDate),title:a.title,body:a.content,shortBody:a.contentSnippet,bodyPlain:function(a){for(var a=a.content.replace(/<script[\\r\\\s\S]*<\/script>/mgi,"").replace(/<\/?[^>]+>/gi,""),b=0;b<e.htmlTags.length;b++)a=a.replace(RegExp("<"+e.htmlTags[b],"gi"),"");return a}(a),shortBodyPlain:a.contentSnippet.replace(/<\/?[^>]+>/gi,""),index:d.inArray(a,this.entries),totalEntries:this.entries.length,
teaserImage:function(a){try{return a.content.match(/(<img.*?>)/gi)[0]}catch(b){return""}}(a),teaserImageUrl:function(a){try{return a.content.match(/(<img.*?>)/gi)[0].match(/src="(.*?)"/)[1]}catch(b){return""}}(a)},this.options.tokens)};e.prototype.getValueForToken=function(a,b){var c=this.getTokenMap(b),d=a.replace(/[\{\}]/g,""),d=c[d];if("undefined"!==typeof d)return"function"===typeof d?d(b,c):d;throw Error("Unknown token: "+a+", url:"+this.url);};d.fn.rss=function(a,b,c){(new e(this,a,b,c)).render();
return this}})(jQuery);

jQuery(document).ready(function($) {
  $('#btn-submit').click(function(e) {
    e.preventDefault();


    // Validate form.
    var validator = $('#recommend').parsley();
    validator.validate();

    if (validator.isValid()) {
      var yourPrice = $('#input-price').val();
      var yourSpeed = $('#input-speed').val();
      var state = $('#input-state option:selected').data('value');
      $('.selected-provider').text($('#input-provider').val());
      $('.selected-price').text('RM' + yourPrice);
      $('.selected-speed').text(yourSpeed + 'Mbps');
      $('.selected-area').text($('#input-state').val());

      $('#landing').fadeOut(300, function() {
        $('.loading').fadeIn(500, function() {
          $(this).fadeOut(300);
        });
      });

      $.getJSON(
          '/broadband/plan-recommendation/best?price=' + yourPrice + '&speed=' +
          yourSpeed + '&state=' + state, function(data) {
            if (data.success && (data.cheaper || data.faster)) {
              if (data.cheaper) {
                var $cheaperProduct = $('.result-card--price');
                var nid = data.cheaper.nid;
                var name = data.cheaper.title;
                var logo = data.cheaper.field_logo;
                var speed = parseInt(data.cheaper.field_speed);
                var price = parseFloat(data.cheaper.field_monthly_fee_rm_);
                var oldPrice = data.cheaper.field_old_monthly_fee_rm_;
                var quota = data.cheaper.field_quota;
                $cheaperProduct.find('.plan-display h3 span').
                    text('RM' + (yourPrice - price).toFixed(2) + '!');
                $cheaperProduct.find('.line--middle').
                    html(price.toFixed(0) + '<span>.' +
                        (parseInt(price * 100 % 100) || '00') + '</span>');
                $cheaperProduct.find('.product-info--name img').
                    attr('src', logo);
                $cheaperProduct.find('.product-name').text(name);
                $cheaperProduct.find('.product-info--price').
                    text(speed + 'Mbps');
                $cheaperProduct.find('.product-info--speed').
                    text(speed + 'Mbps');
                $cheaperProduct.find('.product-info--quota').text(quota);

                // Prepare data for modal.
                $cheaperProduct.find('.btn-apply').
                    data('nid', nid).
                    data('img', logo).
                    data('speed', speed).
                    data('price', price).
                    data('oldPrice', oldPrice).
                    data('quota', quota).
                    data('name', name).                    
                    attr('data-value', name).
                    attr('data-description', 'Get Cheaper Plan');
              }
              else {
                var $price = $('.result-card--price');
                $price.find('.plan-display').hide();
                $price.find('.plan-best').show();
              }

              if (data.faster) {
                var $fasterProduct = $('.result-card--speed');
                var nid = data.faster.nid;
                var name = data.faster.title;
                var logo = data.faster.field_logo;
                var speed = data.faster.field_speed;
                var price = data.faster.field_monthly_fee_rm_;
                var oldPrice = data.faster.field_old_monthly_fee_rm_;
                var quota = data.faster.field_quota;

                $fasterProduct.find('.plan-display h3 span').
                    text((speed - yourSpeed) + 'Mbps!');
                $fasterProduct.find('.line--middle').text(speed);
                $fasterProduct.find('.product-info--name img').
                    attr('src', logo);
                $fasterProduct.find('.product-name').text(name);
                $fasterProduct.find('.product-info--price').
                    text('RM' + price.toFixed(2));
                $fasterProduct.find('.product-info--quota').
                    text(quota);

                // Prepare data for modal.
                $fasterProduct.find('.btn-apply').
                    data('nid', nid).
                    data('img', logo).
                    data('speed', speed).
                    data('price', price).
                    data('oldPrice', oldPrice).
                    data('quota', quota).
                    data('name', name).
                    attr('data-value', name).
                    attr('data-description', 'Get Faster Speeds');
              }
              else {
                var $speed = $('.result-card--speed');
                $speed.find('.plan-display').hide();
                $speed.find('.plan-best').show();
              }

              $('#results-none').hide();
              $('.results').delay(1100).fadeIn(200, function() {
                $('.btn-line').click(function() {
                  $('.results').fadeOut(500, function() {
                    $('#landing').fadeIn();
                  });
                });
              });

              //LAUNCH MODAL
              $('#apply-modal').on('show.bs.modal', function(event) {
                var $modal = $(this);
                var $form = $('#applyForm');
                var $button = $(event.relatedTarget);

                $modal.find('.applying-name h5').text($button.data('name'));
                $modal.find('.applying-img img').
                    attr('src', $button.data('img'));
                $modal.find('.applying-speed').
                    text($button.data('speed') + 'Mbps');
                $modal.find('.applying-price').
                    text('RM' + $button.data('price'));
                $modal.find('.applying-quota').text($button.data('quota'));
                if ($button.data('oldPrice'))
                  $modal.find('.applying-price-old').
                      text('RM' + $button.data('oldPrice'));
                $modal.find('.applying-img img').
                    attr('src', $button.data('img'));
                $modal.find('#btn-bb-submit').
                    attr('data-value', $button.data('name'));

                // Set source.
                var lang = $('html').attr('lang');
                IMDevPropBag.add('SRCREGION', lang + '_bb_recommendation');

                $form.submit(function(e) {
                  e.preventDefault();

                  var $btnSubmit = $form.find('#btn-bb-submit');
                  var params = {};

                  _.each($form.serializeArray(), function(item) {
                    if (typeof params[item.name] ===
                        'undefined') params[item.name] = [];
                    params[item.name].push(item.value);
                  });

                  _.each(_.clone(params), function(item, key) {
                    params[key] = params[key].join(',');
                  });

                  params.productName = $button.data('name') + ', ' +
                      $button.data('speed') + 'Mbps';

                  var region = $('body').data('region');

                  new CampaignFactory(region, undefined, function(factory) {
                    $btnSubmit.text($btnSubmit.data('wait')).
                        attr('disabled', 'disabled');

                    //TODO: factory should getByMachineName instead
                    var cam = factory.getByNid($button.data('nid') + '');

                    cam.setFieldValue('name', params.name);
                    cam.setFieldValue('email_address', params.email_address);
                    cam.setFieldValue('phone_number', params.phone_number);
                    cam.setFieldValue('address', params.address);

                    cam.setFieldValue('bb_coverage_address',
                        $.cookie('bb_coverage_address'));
                    cam.setFieldValue('bb_available_providers',
                        $.cookie('bb_available_providers'));

                    cam.submit(function(res, status) {
                      if (status === 'complete') {
                        window.location = $form.data('thankyou');
                      }
                      else {
                        $btnSubmit.text($btnSubmit.data('Error'));
                        $form.find('input').val('');
                        $modal.modal('hide');
                      }
                    });
                  });
                });
              });
            }
            else {
              $('.results').hide();
              $('#results-none').delay(1100).fadeIn(200, function() {
                $('.articles-section').queue(function() {
                  $(this).addClass('articles-section--fade-in');
                });
                $('.btn-line').click(function() {
                  $('#results-none').fadeOut(500, function() {
                    $('#landing').fadeIn();
                  });
                });
              });
            }
          });

      // RSS Feed.
      var rssUrlLatest = $('.latest').data('rss');
      var rssUrlCards = $('.cards').data('rss');

      var mq = window.matchMedia('(min-width:480px)');

      $('.latest ul').remove();
      $('.cards ul').remove();
      $('.latest').rss(rssUrlLatest, {
        limit: (mq.matches) ? 5 : 3,
        ssl: true,
        entryTemplate: '<li><div class="img-wrap">{teaserImage}</div><a href="{url}" data-value="{title}">{title}</a></li>'
      });

      $('.cards').rss(rssUrlCards, {
        limit: (mq.matches) ? 5 : 3,
        ssl: true,
        entryTemplate: '<li><div class="img-wrap">{teaserImage}</div><a href="{url}" data-value="{title}">{title}</a></li>',
      });
    }
  });

  //SCROLL TO TOP
  $(function() {
    $('.btn-line').click(function() {
      $('html,body').animate({
        scrollTop: $('.container').offset().top,
      }, '1000');
      return false;
    });
  });

});

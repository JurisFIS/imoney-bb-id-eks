// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/affix.js";

$(document).ready(function() {

    $('body').scrollspy({
        offset: 100
    });

    $('#navbar').affix({
        offset: {
            top: function () {
                return (this.top = $('#navbar').outerHeight(true) + 100)
            },
            bottom: function () {
                return (this.bottom = $('#cta-bottom').outerHeight(true) + $('.footer').outerHeight(true))
            }
        }
    });

});
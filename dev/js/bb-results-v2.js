// @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
// @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";

String.prototype.capitalize = function () {
    return this.toLowerCase().replace(/\b./g, function (a) {
        return a.toUpperCase();
    });
};

function cardSubmitCallback(error, eventName, params, callback_params) {
    $(callback_params).text('Applying...').attr('disabled', 'disabled');

    if (eventName == 'complete') {
        $(callback_params).text('Done!');
    }
}

var region = $('body').data('region');
$(document).ready(function () {

    //add animate class to results wrapper depends on data-delay attribute
    $('.results-bb').each(function () {
        var $item = $(this);
        var dataDelay = (Math.random() + 1) * 2000;
        $item.delay(dataDelay).queue(function (next) {
            $item.children('.results-wrapper').addClass('animated bounceInUp');
            $item.children('.loading-wrap').fadeOut(200);
            next();
        });

    });

    $('#apply-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var planImg = button.data('image'); // Extract info from data-* attributes
        var planName = button.data('title');
        var planSpeed = button.data('speed');
        var planQuota = button.data('quota');
        var planPrice = button.data('price');
        var planNid = button.data('nid');
        var planApply = button.data('apply');
        var modal = $(this);
        var translateMonth = modal.data('translate_month');

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        modal.find('form').attr('action', planApply);
        modal.find('.applying-img').html('<img src="' + planImg + '" height="30">'); // Commented by thayub
        modal.find('.applying-name h5').text(planName);
        modal.find('.applying-speed').text(planSpeed + " Mbps");
        modal.find('.applying-quota').text(planQuota);
        modal.find('.applying-price').html(planPrice + '<span class="extra-info">/' + translateMonth + '</span>');
        modal.find('.modal-footer button').attr('data-value', button.data('title'));


        // Set source.
        var lang = $('html').attr('lang');
        IMDevPropBag.add('SRCREGION', lang + '_bb_listing');

        $('#applyForm').submit(function(e) {
            e.preventDefault();
            var that = this; //The form object

            var btnSubmit = $(that).find('#btn-bb-submit');
            var params = {};

            _.each($(that).serializeArray(), function (item) {
                if (typeof params[item.name] == 'undefined') params[item.name] = [];
                params[item.name].push(item.value);
            });

            _.each(_.clone(params), function (item, key) {
                params[key] = params[key].join(',');
            });

            params.productName = planName + ', ' + planSpeed + 'Mbps';

            // Set source.
            var lang = $('html').attr('lang');
            IMDevPropBag.add('SRCREGION', lang + '_bb_smartsearch_results_v2');

            new CampaignFactory(region, undefined, function(factory) {
                btnSubmit.text(btnSubmit.data('wait')).attr('disabled', 'disabled');

                //TODO: factory should getByMachineName instead
                var cam = factory.getByNid(planNid + '');

                cam.setFieldValue('name', params.name);
                cam.setFieldValue('email_address', params.email_address);
                cam.setFieldValue('phone_number', params.phone_number);
                cam.setFieldValue('address', params.address);

                if(region === 'my') {
                    cam.setFieldValue('bb_coverage_address', $.cookie('bb_coverage_address'));
                    cam.setFieldValue('bb_available_providers', $.cookie('bb_available_providers'));
                }

                cam.submit(function(res, status) {
                    if(status == 'complete') {
                        window.location = $(that).data('thankyou');
                    }
                    else {
                        btnSubmit.text(btnSubmit.data('Error'));
                        $(that).find('input').val('');
                        modal.modal('hide');
                    }
                })
            });
        });
    });

    //fix unassign modal-open class to body for second modal popup for mobile offset
    var body = $('body')

      .on('shown.bs.modal', '.modal', function () {
          body.addClass('modal-open')
      })
      .on('hidden.bs.modal', '.modal', function () {
          body.removeClass('modal-open')
      });

    //Init tracking for shown cards
    //TODO: Target only shown cards. Do not rebind all components on the page to avoid duplicate event tracking.
    imuInit('#suggested-broadband button');

    // submit form when select something in skip modal
    $('[name="result-skip-feedback"]').change(function () {
        $('.results-feedback').delay(700).queue(function (next) {
            // $(this).submit();
            $('#skip-link').modal('hide');
            $('#needs-content').modal('show');
            next();
        });
    })
});
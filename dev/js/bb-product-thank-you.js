// jQuery RSS
!function(a){"use strict";var b=function(b,c,d,e){this.target=b,this.url=c,this.html=[],this.effectQueue=[],this.options=a.extend({ssl:!1,host:"www.feedrapp.info",limit:null,key:null,layoutTemplate:"<ul>{entries}</ul>",entryTemplate:'<li><a href="{url}">[{author}@{date}] {title}</a><br/>{shortBodyPlain}</li>',tokens:{},outputMode:"json",dateFormat:"dddd MMM Do",dateLocale:"en",effect:"show",offsetStart:!1,offsetEnd:!1,error:function(){console.log("jQuery RSS: url doesn't link to RSS-Feed")},onData:function(){},success:function(){}},d||{}),this.callback=e||this.options.success};b.htmlTags=["doctype","html","head","title","base","link","meta","style","script","noscript","body","article","nav","aside","section","header","footer","h1-h6","hgroup","address","p","hr","pre","blockquote","ol","ul","li","dl","dt","dd","figure","figcaption","div","table","caption","thead","tbody","tfoot","tr","th","td","col","colgroup","form","fieldset","legend","label","input","button","select","datalist","optgroup","option","textarea","keygen","output","progress","meter","details","summary","command","menu","del","ins","img","iframe","embed","object","param","video","audio","source","canvas","track","map","area","a","em","strong","i","b","u","s","small","abbr","q","cite","dfn","sub","sup","time","code","kbd","samp","var","mark","bdi","bdo","ruby","rt","rp","span","br","wbr"],b.prototype.load=function(b){var c="http"+(this.options.ssl?"s":""),d=c+"://"+this.options.host,e=d+"?callback=?&q="+encodeURIComponent(this.url);this.options.offsetStart&&this.options.offsetEnd&&(this.options.limit=this.options.offsetEnd),null!==this.options.limit&&(e+="&num="+this.options.limit),null!==this.options.key&&(e+="&key="+this.options.key),a.getJSON(e,b)},b.prototype.render=function(){var b=this;this.load(function(c){try{b.feed=c.responseData.feed,b.entries=c.responseData.feed.entries}catch(a){return b.entries=[],b.feed=null,b.options.error.call(b)}var d=b.generateHTMLForEntries();if(b.target.append(d.layout),0!==d.entries.length){a.isFunction(b.options.onData)&&b.options.onData.call(b);var e=a(d.layout).is("entries")?d.layout:a("entries",d.layout);b.appendEntriesAndApplyEffects(e,d.entries)}b.effectQueue.length>0?b.executeEffectQueue(b.callback):a.isFunction(b.callback)&&b.callback.call(b)})},b.prototype.appendEntriesAndApplyEffects=function(b,c){var d=this;a.each(c,function(a,c){var e=d.wrapContent(c);"show"===d.options.effect?b.before(e):(e.css({display:"none"}),b.before(e),d.applyEffect(e,d.options.effect))}),b.remove()},b.prototype.generateHTMLForEntries=function(){var b=this,c={entries:[],layout:null};return a(this.entries).each(function(){var f,a=this,d=b.options.offsetStart,e=b.options.offsetEnd;d&&e?index>=d&&index<=e&&b.isRelevant(a,c.entries)&&(f=b.evaluateStringForEntry(b.options.entryTemplate,a),c.entries.push(f)):b.isRelevant(a,c.entries)&&(f=b.evaluateStringForEntry(b.options.entryTemplate,a),c.entries.push(f))}),this.options.entryTemplate?c.layout=this.wrapContent(this.options.layoutTemplate.replace("{entries}","<entries></entries>")):c.layout=this.wrapContent("<div><entries></entries></div>"),c},b.prototype.wrapContent=function(b){return a(0!==a.trim(b).indexOf("<")?"<div>"+b+"</div>":b)},b.prototype.applyEffect=function(a,b,c){var d=this;switch(b){case"slide":a.slideDown("slow",c);break;case"slideFast":a.slideDown(c);break;case"slideSynced":d.effectQueue.push({element:a,effect:"slide"});break;case"slideFastSynced":d.effectQueue.push({element:a,effect:"slideFast"})}},b.prototype.executeEffectQueue=function(a){var b=this;this.effectQueue.reverse();var c=function(){var d=b.effectQueue.pop();d?b.applyEffect(d.element,d.effect,c):a&&a()};c()},b.prototype.evaluateStringForEntry=function(b,c){var d=b,e=this;return a(b.match(/(\{.*?\})/g)).each(function(){var a=this.toString();d=d.replace(a,e.getValueForToken(a,c))}),d},b.prototype.isRelevant=function(a,b){var c=this.getTokenMap(a);return!this.options.filter||(!this.options.filterLimit||this.options.filterLimit!==b.length)&&this.options.filter(a,c)},b.prototype.getFormattedDate=function(a){if(this.options.dateFormatFunction)return this.options.dateFormatFunction(a);if("undefined"!=typeof moment){var b=moment(new Date(a));return b=b.locale?b.locale(this.options.dateLocale):b.lang(this.options.dateLocale),b.format(this.options.dateFormat)}return a},b.prototype.getTokenMap=function(c){if(!this.feedTokens){var d=JSON.parse(JSON.stringify(this.feed));delete d.entries,this.feedTokens=d}return a.extend({feed:this.feedTokens,url:c.link,author:c.author,date:this.getFormattedDate(c.publishedDate),title:c.title,body:c.content,shortBody:c.contentSnippet,bodyPlain:function(a){for(var c=a.content.replace(/<script[\\r\\\s\S]*<\/script>/gim,"").replace(/<\/?[^>]+>/gi,""),d=0;d<b.htmlTags.length;d++)c=c.replace(new RegExp("<"+b.htmlTags[d],"gi"),"");return c}(c),shortBodyPlain:c.contentSnippet.replace(/<\/?[^>]+>/gi,""),index:a.inArray(c,this.entries),totalEntries:this.entries.length,teaserImage:function(a){try{return a.content.match(/(<img.*?>)/gi)[0]}catch(a){return""}}(c),teaserImageUrl:function(a){try{return a.content.match(/(<img.*?>)/gi)[0].match(/src="(.*?)"/)[1]}catch(a){return""}}(c)},this.options.tokens)},b.prototype.getValueForToken=function(a,b){var c=this.getTokenMap(b),d=a.replace(/[\{\}]/g,""),e=c[d];if("undefined"!=typeof e)return"function"==typeof e?e(b,c):e;throw new Error("Unknown token: "+a+", url:"+this.url)},a.fn.rss=function(a,c,d){return new b(this,a,c,d).render(),this}}(jQuery);


$(document).ready(function(){	

	function checkHeight(className){
		var thisHeight, highest = 0;
		if($(window).width() >= 992) {
			$(className).each(function(){

				thisHeight = $(this).innerHeight();

				if(thisHeight > highest) {
					highest = thisHeight;
				}
			});
			$(className).css('height', highest);
		}
	}

	$(window).resize(function(){

		if($(window).width() >= 992) {
			$('.related').removeAttr('style');
			setTimeout(checkHeight('.related'),5);
		} else {
			$('.related').removeAttr('style');
		}
	});



	$('.envelope-open, .envelope-close, .letter, .envelope-wrap, .tick-green, .envelope-container, .tick-icon').addClass('animate');

	var rssUrlLatest = $('.latest').data('rss');
	var rssUrlCards = $('.cards').data('rss');

	var mq = window.matchMedia('(min-width:480px)');

	$('.latest').rss(rssUrlLatest,{
		limit: (mq.matches) ? 5 : 3,
		ssl: true,			
		entryTemplate: '<li><div class="img-wrap">{teaserImage}</div><a href="{url}" data-value="{title}">{title}</a></li>'
		},function() {
			imuInit('.latest a');
			$('.latest .preloader').addClass('fadeOut');
			checkHeight('.related');
			console.log(checkHeight('.related'));		
	});

	$('.cards').rss(rssUrlCards,{
		limit: (mq.matches) ? 5 : 3,
		ssl: true,		
		entryTemplate: '<li><div class="img-wrap">{teaserImage}</div><a href="{url}" data-value="{title}">{title}</a></li>',		
	},function() {imuInit('.cards a'); $('.cards .preloader').addClass('fadeOut');});

})
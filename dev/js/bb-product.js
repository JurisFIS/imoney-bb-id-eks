// @codekit-prepend "top-mainnav.js";

var region = $('body').data('region');
jQuery(document).ready(function ($) {
	//get and clone to duplicate it
	$('header img').clone().appendTo('.sticky--plan-img');

	$('.apply .btn-success').clone().appendTo('.sticky--plan-btn');

	var planName = $('.header--broadband h1').text();

	$('.sticky--plan-name').text(planName);

	//function to calculate black area position
	function FeaturePosition(className) {
		var featurePosition = $(className).offset().top;
		return featurePosition + $(className).outerHeight();
	}

	//get the position at start
	var posFeatures = FeaturePosition('.bb-keypoints');

	//recalculate the position on window resize
	$(window).resize(function () {
		posFeatures = FeaturePosition('.bb-keypoints');
	});

	//activate the sticky header on body scroll
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= posFeatures) {
			$('.sticky-bb-header').addClass('slidein');
		}
		else {
			$('.sticky-bb-header').removeClass('slidein');
		}
	});

	//change header background color
	var logosrc = $(".logo-holder--img").data('logo');

	$('.header--broadband').each(function() {
		$(this).css('background-color', $(this).data('color')).animate({opacity: 1.0}, 2000);
	});

	//reduce font size when the product title is too long
	var productTitle = $('header h1');
	if(productTitle.text().length >= 42) {

		productTitle.css('font-size', '30px');

		var mq = window.matchMedia( "(min-width: 768px) and (max-width: 1199px)" );
		if (mq.matches) {
			productTitle.css({'padding-top' : '26px', 'padding-right' : '300px'})
		}
	}

	//reduce font size when the product price is too long
	var productPrice = $('.bb-keypoints__price');
	if(productPrice.text().length >= 11) {

		var mq = window.matchMedia( "(min-width: 516px)" );
		if (mq.matches) {
			productPrice.css('font-size', '22px');
		}
	}

	//***********************************
	//  Broadband Package Selection
	//***********************************

	// select default broadband package
	var bbpack = $('input[name=package]:radio');
	if(bbpack.is(':checked') === false) {
		bbpack.filter(':first').prop('checked', true);
	};

	function totalPackagePrice () {
		var $checked = $('input[name=package]:checked');
		var selectedPackage = Number($checked.data('addon'));

		$('#price-package h4').text($checked.data('abs'));
		if (selectedPackage < 0) {
			$('#price-bb').addClass('discount');
		} else {
			$('#price-bb').removeClass('discount');
		}
		$('#price-package span').text($checked.data('addontype'));
		$('#price-total h4').html('<span>Total</span> ' + $checked.data('sum'));
		$('.apply .btn').data('price', $checked.data('sum'));
	};
	totalPackagePrice();
	
	// change text according to selected tv package
	bbpack.change(function () {
		totalPackagePrice();
	});


	//***********************************
	//  Modal : Apply Form
	//***********************************
	// @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
	// @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";

	$('#apply-modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var planImg = button.data('image'); // Extract info from data-* attributes
		var planName = button.data('title');
		var planSpeed = button.data('speed');
		var planQuota = button.data('quota');
		var planPrice = button.data('price');
		var planOldPrice = button.data('old_price');
		var planNid = button.data('nid');
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this);
		modal.find('.applying-img').html('<img src="' + planImg + '" height="30">'); // Commented by thayub
		modal.find('.applying-name h5').text(planName);
		modal.find('.applying-speed').text(planSpeed + " Mbps");
		modal.find('.applying-quota').text(planQuota);
		$applying = modal.find('.applying-price');
		$applying.html(planPrice + '<span class="extra-info">/' + $applying.data('month') + '</span>');
    if (planOldPrice !== '-') modal.find('.applying-price-old').
        html(planOldPrice + '/' + $applying.data('month') + '</span>');

		$('#applyForm').submit(function(e) {
			e.preventDefault();
			var that = this; //The form object
			var btnSubmit = $(that).find('#btn-bb-submit');
			var params = {};

			_.each($(that).serializeArray(), function (item) {
				if (typeof params[item.name] === 'undefined') params[item.name] = [];
				params[item.name].push(item.value);
			});

			_.each(_.clone(params), function (item, key) {
				params[key] = params[key].join(',');
			});

			params.productName = planName + ', ' + planSpeed + 'Mbps';

			// Set source.
			var lang = $('html').attr('lang');
			IMDevPropBag.add('SRCREGION', lang + '_bb_product');

			new CampaignFactory(region, undefined, function(factory) {
				btnSubmit.text('Please wait...').attr('disabled', 'disabled');

				//TODO: factory should getByMachineName instead
				var cam = factory.getByNid(planNid + '');

				cam.setFieldValue('name', params.name);
				cam.setFieldValue('email_address', params.email_address);
				cam.setFieldValue('phone_number', params.phone_number);
				cam.setFieldValue('address', params.address);

				if (region === 'ph') {
					cam.setFieldValue('state', params.city);
				}

                var bb_coverage_address = $.cookie('bb_coverage_address');
                var bb_available_providers = $.cookie('bb_available_providers');

                cam.setFieldValue('bb_coverage_address', bb_coverage_address);
                cam.setFieldValue('bb_available_providers', bb_available_providers);

				cam.submit(function(res, status) {
					if(status === 'complete') {
						window.location = $(that).data('thankyou');
						/*
						 btnSubmit.text(btnSubmit.data('proceed')).removeAttr('disabled');
						 $(that).find('input').val('');
						 modal.modal('hide');

						 $('#modal-broadband-thankyou').modal('show');
						 button.addClass('bb-applied').text(btnSubmit.data('applied'));
						 */
					}
					else {
						btnSubmit.text("Error");
						$(that).find('input').val('');
						modal.modal('hide');
					}
				})
			});
		});
	});
});
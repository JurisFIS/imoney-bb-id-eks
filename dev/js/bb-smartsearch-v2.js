// @codekit-prepend "../../build/js/slick/slick.min.js";
// @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
// @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";

function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if (decodeURIComponent(pair[0]) == variable) {
			return decodeURIComponent(pair[1]);
		}
	}
}

//smart search title
var ssTitle = $('#ss-title');
$(document).ready(function(){
	if (!getQueryVariable('REASON')) {
		ssTitle.text(ssTitle.data('noreason'));
	}
});

//slick
var slideQuestions = $('.smartsearch'),
	slideProgressBar = $('.ss-progressbar'),
	slideIndicator = $('.smartsearch-indicator ul'),
	slideButtons = $('.smartsearch-indicator span'),
	totalSlides;

slideQuestions.slick({
	accessibility: false,
  infinite: false,
  draggable: false,
  swipe: false,
  arrows: false,
  speed: 300,
  adaptiveHeight: true,
  initialSlide: getQueryVariable('REASON')? 1: 0,
  onAfterChange:function(slider, i){
		//remove all active class
		$(slideButtons).removeClass('icn_highlight');
		$(slideButtons).parent('li').removeClass('icn_highlight_done');

		//set active class for current slide
		$(slideButtons).eq(i).addClass('icn_highlight');

		for(var num = i-1; num >= 0; num--) {
			$(slideButtons).eq(num).addClass('icn_highlight').css('opacity', '0.2');
			$(slideButtons).eq(num).parent('li').addClass('icn_highlight_done');
		}

		//progress bar
		var currentSlide = slideQuestions.slickCurrentSlide() + 1;
		if($(slideQuestions).length === 0 ) {
			slideProgressBar.width('100%');
		} else {
			slideProgressBar.width(currentSlide/totalSlides * 100 + '%');
		}

		if(slideQuestions.slickCurrentSlide() == 5) {
			$('.capture input, .capture button').removeAttr('tabindex');
		}
	},
	onInit: function(slider, i){
		slideQuestions.animate({'opacity': 1}, 400);
		$('.smartsearch-indicator').animate({'opacity': 1}, 400);
		totalSlides = $('.slick-slide').length - $('.slick-cloned').length;
		slideProgressBar.width(1/totalSlides * 100 + '%');
		//console.log(slideQuestions.slickCurrentSlide());
		// to re-select first buttons on first slide
		// $('.btns-spending li:first-child > label').click();
		if (getQueryVariable('REASON')) {
			// set active class to first slide to indicator
			$(slideButtons).eq(0).addClass('icn_highlight').css('opacity', '0.2');
			$(slideButtons).eq(0).parent('li').addClass('icn_highlight_done');
			$(slideButtons).eq(1).addClass('icn_highlight');
		} else {
			$(slideButtons).eq(0).addClass('icn_highlight');
			$('#purpose-dropdown').focus();
		}
	}
});

// icon indicator
$(slideIndicator).slick({
	slide: 'li',
	asNavFor: '.smartsearch',
	slidesToShow: 5,
	slidesToScroll: 1,
	dots: true
});

// set active class to first slide to indicator
// $(slideButtons).eq(0).addClass('icn_highlight').css('opacity', '0.2');
// $(slideButtons).eq(0).parent('li').addClass('icn_highlight_done');
// $(slideButtons).eq(1).addClass('icn_highlight');

$('input[name="REASON"]').on('click',function(){
	if($('input[name="REASON"]').is(':checked')) {
		$(slideQuestions).delay(500).queue(function(next){
			//$(slideQuestions).slickNext();
			next();
		});
	}
});

// go to next slide after select an answer
$('.purpose input, #home, .habit input, .userNum input, .location input, .staticIP input, .btn-next').on('click', function(){

	//scroll to top
	$('html, body').animate({
		scrollTop : $('.bb-smartsearch-header').offset().top + $('.bb-smartsearch-header').outerHeight()
	});

	$(slideQuestions).delay(500).queue(function(next){
		$(slideQuestions).slickNext();
		next();
	});
});

// slide down more question if user select My Office
$(".hiddenQuestion").hide();

$('input:radio[name="broadbandFor"]').change(
	function(){
	if ($(this).val() == 'Office') {
		$(".hiddenQuestion").show().find('[name="StaticIP"]').prop('checked',false);

		$(slideQuestions).slickSetOption('responsive', true, true);
	}
	else {
		$(".hiddenQuestion").hide();

		$('#staticIP-no').prop('checked',true);
		$(slideQuestions).slickSetOption('responsive', true, true);
	}
});

//change usage habit icons into a single button in mobile

$(document).ready(function(){
	var mq = window.matchMedia('all and (max-width: 768px)');
	if(mq.matches) {
		$('label[for=lightUsage]').append('<br><span>Web browsing, Emails, Facebook &amp; Twitter</span>');
		$('label[for=moderateUsage]').append('<br><span>Light usage + Spotify music &amp; Skype calls</span>');
		$('label[for=heavyUsage]').append('<br><span>Moderate usage + Youtube, Downloading &amp; File transferring</span>');
		$('label[for=extremeUsage]').append('<br><span>Heavy usage + Video conference &amp; Online gaming</span>');
	} else {
		//do nothing
	}
});

// capture form validation
var ssMachineName = {
	'my': 'MY_BB_Maybank_Islamic_Broadband_Visual_SmartSearch',
	'id': 'ID_BB_iMoney_Broadband_Visual_SmartSearch',
	'ph': 'PH_BB_TM_Broadband_Visual_SmartSearch',
	'sg': 'SG_BB_iMoney_Broadband_Visual_SmartSearch'
};
var region = $('body').data('region');
// prefer-to-talk form validation
$('.PreferToTalk').attr('data-vertical', 'Broadband');
$('.btn-callback').on('click', function(e){
	$('#prefer-to-talk-form').parsley().validate('block1');

	if($('#prefer-to-talk-form').parsley().isValid('block1')) {
		var form = $('#prefer-to-talk-form');

		// Set source.
		var lang = $('html').attr('lang');
		IMDevPropBag.add('SRCREGION', lang + '_bb_callmeback');

		new CampaignFactory('my', undefined, function(factory) {
			$('#filter_bb button').html('Please wait...').attr('disabled', 'disabled');

			//TODO: factory should getByMachineName instead
			var cam = factory.get('54c1b99ddaaf45894b8b4567');

			cam.setFieldValue('name', $(form).find("input[name='name']").val());
			cam.setFieldValue('email_address', $(form).find("input[name='email_address']").val());
			cam.setFieldValue('phone_number', $(form).find("input[name='phone_number']").val());

      if(region === 'my') {
        cam.setFieldValue('bb_coverage_address', $.cookie('bb_coverage_address'));
        cam.setFieldValue('bb_available_providers', $.cookie('bb_available_providers'));
      }

			cam.submit(function(res, status) {
				var $button = $(form).find('button');
				if(status == 'complete') {
					$button.html($button.data('done'));

					$('.PreferToTalk').modal('hide');
					$('#PreferToTalkThankYouBB').modal('show');

					//fix unassign modal-open class to body for second modal popup for mobile offset
					var body = $('body')
						.on('shown.bs.modal','.modal', function(){ body.addClass('modal-open')})
						.on('hidden.bs.modal','.modal', function(){ body.removeClass('modal-open')});

				}
				else {
					$button.html($button.data('saving')).attr('disabled', 'disabled');
				}
			})
		});
	}

	return false;
});

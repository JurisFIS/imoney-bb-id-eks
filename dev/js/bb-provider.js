
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js";

var region = $('body').data('region');
$(document).ready(function () {

	//function to calculate black area position
	function FeaturePosition(className) {
	    var featurePosition = $(className).offset().top;
	    return featurePosition + $(className).outerHeight();
	}

	//get the position at start
	var posFeatures = FeaturePosition('.general-info');

	//recalculate the position on window resize
	$(window).resize(function () {
	    posFeatures = FeaturePosition('.general-info');
	});

	//hide button in sticky anchor if no such div
	if (!$('#promo').length) {
	    $('a[href="#promo"]').remove();
	}

	if (!$('#bt').length) {
	    $('a[href="#bt"]').remove();
	}

	//activate the sticky anchor
	$('.sticky-cc-anchor').addClass('slidein');

	//set position to deactivate sticky anchor
	$(window).scroll(function () {
	    var scroll = $(window).scrollTop();

	    if (scroll >= posFeatures) {
	        $('.sticky-cc-anchor').removeClass('slidein');
	    }
	    else {
	        $('.sticky-cc-anchor').addClass('slidein');
	    }
	});

	//make anchoring scroll smoother
  var $root = $('html, body');

  $('.sticky-cc-anchor a').click(function() {
      $root.animate({
          scrollTop: $( $.attr(this, 'href') ).offset().top
      }, 500);
      return false;
  });

  //promo cc - show card feature on click
  $('.show-info').on('click','a', function(e) {
  	e.preventDefault();

    var $button = $(this);
    var $featurePromo = $button.parents(".provider").find('.provider__promo');
    $button.html("+ " + ($featurePromo.hasClass('slideOut') ? $button.data('show') : $button.data('hide')));
    $featurePromo.toggleClass('slideOut');
  });


  //submit APPLY MODAL

  $('#apply-modal').on('show.bs.modal', function (event) {
  	var button = $(event.relatedTarget); // Button that triggered the modal
  	var planImg = button.data('image'); // Extract info from data-* attributes
  	var planName = button.data('title');
  	var planSpeed = button.data('speed');
  	var planQuota = button.data('quota');
  	var planPrice = button.data('price');
  	var planOldPrice = button.data('old_price');
  	var planNid = button.data('nid');
  	var planApply = button.data('apply');

  	// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  	var modal = $(this);
  	modal.find('form').attr('action', planApply);
  	modal.find('.applying-img').html('<img src="' + planImg + '" height="30">'); // Commented by thayub
  	modal.find('.applying-name h5').text(planName);
  	modal.find('.applying-speed').text(planSpeed + " Mbps");
  	modal.find('.applying-quota').text(planQuota);
  	modal.find('.applying-price').html(planPrice + '<span class="extra-info">' + modal.data('month') + '</span>');
    var oldPriceDiv = modal.find('.applying-price-old');
    if (planOldPrice !== '-') oldPriceDiv.html(planOldPrice + modal.data('month'));
    else oldPriceDiv.html('');
  	modal.find('.modal-footer button').attr('data-value', button.data('title'));

  	$('#applyForm').submit(function(e) {
  		e.preventDefault();
  		var that = this; //The form object

  		var btnSubmit = $(that).find('#btn-bb-submit');
  		var params = {};

  		_.each($(that).serializeArray(), function (item) {
  			if (typeof params[item.name] == 'undefined') params[item.name] = [];
  			params[item.name].push(item.value);
  		});

  		_.each(_.clone(params), function (item, key) {
  			params[key] = params[key].join(',');
  		});

  		params.productName = planName + ', ' + planSpeed + 'Mbps';

		// Set source.
		var lang = $('html').attr('lang');
		IMDevPropBag.add('SRCREGION', lang + '_bb_provider');

  		new CampaignFactory(region, undefined, function(factory) {
  			btnSubmit.text('Please wait...').attr('disabled', 'disabled');

  			//TODO: factory should getByMachineName instead
  			var cam = factory.getByNid(planNid + '');

  			cam.setFieldValue('name', params.name);
  			cam.setFieldValue('email_address', params.email_address);
  			cam.setFieldValue('phone_number', params.phone_number);
			cam.setFieldValue('address', params.address);

			if (region === 'ph') {
				cam.setFieldValue('state', params.city);
			}

  			cam.submit(function(res, status) {
  				if(status == 'complete') {
  					window.location = $(that).data('thankyou');
  				}
  			})
  		});
  	});
  });

});
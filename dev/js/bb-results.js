// @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
// @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";

String.prototype.capitalize = function () {
    return this.toLowerCase().replace(/\b./g, function (a) {
        return a.toUpperCase();
    });
};

function cardSubmitCallback(error, eventName, params, callback_params) {
    $(callback_params).text('Applying...').attr('disabled', 'disabled');

    if (eventName === 'complete') {
        $(callback_params).text('Done!');
    }
}
var region = $('body').data('region');
$(document).ready(function () {
    var userData = IMPropBag.getAllProps();
    var encrypted = window.location.href.replace(/(.*)c=(.*?)(&(.*)|$)/g, '$2');
    var userInfo = JSON.parse(CryptoJS.AES.decrypt(decodeURIComponent(encrypted), 'imoney').toString(CryptoJS.enc.Utf8));
    applyJApp = function ($this) {
        var params = userData;
        params.REASON = "";
        params.productName = $this.find('.bb-name').data('name') + ', ' + $this.find('.bb-speed').html();
        params.nid = $this.find('.bb-nid').data('name');
        params = _.extend(params, userInfo);

        // Set source.
        var lang = $('html').attr('lang');
        IMDevPropBag.add('SRCREGION', lang + '_bb_smartsearch_results');

        new CampaignFactory(region, undefined, function(factory) {
            var $buttonProceed = $this.find('button.btn-apply');
            $buttonProceed.html('Please wait...').attr('disabled', 'disabled');

            //TODO: factory should getByMachineName instead
            var cam = factory.getByNid(params.nid + '');

            cam.setFieldValue('name', params.name);
            cam.setFieldValue('email_address', params.email_address);
            cam.setFieldValue('phone_number', params.phone_number);

            if(region === 'my') {
                cam.setFieldValue('bb_coverage_address', $.cookie('bb_coverage_address'));
                cam.setFieldValue('bb_available_providers', $.cookie('bb_available_providers'));
            }

            cam.submit(function(res, status) {
                if(status === 'complete') {
                    $buttonProceed.text('Applied');
                    window.location = $this.data('thankyou');
                }
                else {
                    $buttonProceed.text('Error');
                }
            })
        });
    };

    $('.results-bb').each(function () {
        var $item = $(this);
        var dataDelay = (Math.random() + 1) * 2000;
        $item.delay(dataDelay).queue(function (next) {
            $item.children('.results-wrapper').addClass('animated bounceInUp');
            $item.children('.loading-wrap').fadeOut(200);
            next();
        });

    });

    $('.button-wrapper .btn-apply').click(function (e) {
        if (!$(this).attr('href')) {
            var $applyButton = $(this);
            $applyButton.text($applyButton.data('wait')).attr('disabled', 'disabled');
            var $applyProduct = $applyButton.closest('.results-bb');
            if (userData.state === 'Kuala Lumpur' || userData.state === 'Selangor') {
                e.preventDefault();
            }
            applyJApp($applyProduct);
        }
    });

    $('#done-btn').on('click', function () {

        $('#apply-modal').modal('hide');
        $('#related-content').modal({
            show : true,
            backdrop: 'static',
            keyboard : false
        });
    });

    //fix unassign modal-open class to body for second modal popup for mobile offset
    var body = $('body')

      .on('shown.bs.modal', '.modal', function () {
          body.addClass('modal-open')
      })
      .on('hidden.bs.modal', '.modal', function () {
          body.removeClass('modal-open')
      });

    //Init tracking for shown cards
    //TODO: Target only shown cards. Do not rebind all components on the page to avoid duplicate event tracking.
    imuInit('#suggested-broadband button');

    // submit form when select something in skip modal
    $('[name="result-skip-feedback"]').change(function () {
        $('.results-feedback').delay(700).queue(function (next) {
            // $(this).submit();
            $('#skip-link').modal('hide');
            $('#needs-content').modal('show');
            next();
        });
    })
});
var startTime = Date.now();
jQuery(document).ready(function ($) {
    // Sticky filter bar.
    var mixController = {};
    var tabPosition = $('.cd-tab-filter-wrapper').offset().top;
    var stickyProcessor = function () {
        var faqPosition = $('main + .container').offset().top;
        var currentPosition = $(window).scrollTop();
        if (currentPosition > tabPosition && currentPosition < (faqPosition - 80)) {
            $('main').addClass('is-fixed');
        }
        else {
            $('main').removeClass('is-fixed');
        }
    };

    stickyProcessor();
    $(window).scroll(function () {
        stickyProcessor();
    });

    // Smartsearch form.
    $('#purpose-bottom').change(function() {
        var button_disable = !$(this).val();
        $('#form-bb-bottom button').prop('disabled', button_disable);
    });

    // Popup.
    $('#apply-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var planImg = button.data('image'); // Extract info from data-* attributes
        var planName = button.data('title');
        var planSpeed = button.data('speed');
        var planQuota = button.data('quota');
        var planPrice = button.data('price');
        var planOldPrice = button.data('old_price');
        var planNid = button.data('nid');
        var planApply = button.data('apply');
        var modal = $(this);
        var translateMonth = modal.data('translate_month');

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        modal.find('form').attr('action', planApply);
        modal.find('.applying-img').html('<img src="' + planImg + '" height="30">'); // Commented by thayub
        modal.find('.applying-name h5').text(planName);
        modal.find('.applying-speed').text(planSpeed + " Mbps");
        modal.find('.applying-quota').text(planQuota);
        modal.find('.applying-price').html(planPrice + '<span class="extra-info">/' + translateMonth + '</span>');
        var oldPriceDiv = modal.find('.applying-price-old');
      if (planOldPrice !== '-') oldPriceDiv.html(
          planOldPrice + '/' + translateMonth);
      else oldPriceDiv.html('');
        modal.find('.modal-footer button').attr('data-value', button.data('title'));


        // Set source.
        var lang = $('html').attr('lang');
        IMDevPropBag.add('SRCREGION', lang + '_bb_listing');

        $('#applyForm').submit(function(e) {
            e.preventDefault();
            var that = this; //The form object

            var btnSubmit = $(that).find('#btn-bb-submit');
            var params = {};

            _.each($(that).serializeArray(), function (item) {
                if (typeof params[item.name] == 'undefined') params[item.name] = [];
                params[item.name].push(item.value);
            });

            _.each(_.clone(params), function (item, key) {
                params[key] = params[key].join(',');
            });

            params.productName = planName + ', ' + planSpeed + 'Mbps';

            var region = $('body').data('region');

            new CampaignFactory(region, undefined, function(factory) {
                btnSubmit.text(btnSubmit.data('wait')).attr('disabled', 'disabled');

                //TODO: factory should getByMachineName instead
                var cam = factory.getByNid(planNid + '');

                cam.setFieldValue('name', params.name);
                cam.setFieldValue('email_address', params.email_address);
                cam.setFieldValue('phone_number', params.phone_number);
                cam.setFieldValue('address', params.address);

                if (region === 'ph') {
                    cam.setFieldValue('state', params.city);
                }

                if(region === 'my') {
                    cam.setFieldValue('bb_coverage_address', $.cookie('bb_coverage_address'));
                    cam.setFieldValue('bb_available_providers', $.cookie('bb_available_providers'));
                }

                cam.submit(function(res, status) {
                    if(status == 'complete') {
                        window.location = $(that).data('thankyou');
                        /*
                         btnSubmit.text(btnSubmit.data('proceed')).removeAttr('disabled');
                         $(that).find('input').val('');
                         modal.modal('hide');

                         $('#modal-broadband-thankyou').modal('show');
                         button.addClass('bb-applied').text(btnSubmit.data('applied'));
                         */
                    }
                    else {
                        btnSubmit.text(btnSubmit.data('Error'));
                        $(that).find('input').val('');
                        modal.modal('hide');
                    }
                })
            });
        });
    });

    $('#suggested-broadband').mixItUp({
        load: {
        },
        callbacks: {
            onMixEnd: function (state) {
                if (state.totalShow) {
                    $('.cd-results-count').text(state.totalShow);
                    $('.cd-fail-message').stop().fadeOut(200, function() {
                        $('.cd-success-message').stop().fadeIn(200);
                    });
                }
                else {
                    $('.cd-success-message').stop().fadeOut(200, function() {
                        $('.cd-fail-message').stop().fadeIn(200);
                    });
                }
            },
            onMixLoad: function () {
                $('#suggested-broadband').find('img').each(function () {
                    var src = $(this).data('src');
                    $(this).attr('src', src);
                });

                // Debug loading time.
                var workingTime = Date.now();
                console.log("Need " + (workingTime - startTime) / 1000 + " seconds since javascript can work");
                console.log("Need " + (workingTime - veryFirst) / 1000 + " seconds since page is loaded");

                // Show filter bar when ready to be filtered.
                $('.cd-tab-filter-wrapper, .cd-filter-trigger').animate({opacity: 1}, 200);

                // Navbar filter processor.
                $('.cd-filters a').on('click', function (e) {
                    e.preventDefault();
                    $('.cd-filters a').removeClass('selected');
                    $(this).addClass('selected');
                });

                // Filter panel processor.
                $('.cd-filter-trigger').on('click', function () {
                    // Show filter panel.
                    $('.cd-filter-trigger, .cd-filter, .cd-tab-filter, .cd-gallery').each(function () {
                        $(this).addClass('filter-is-visible');
                    });
                });
                $('.cd-filter .cd-close').on('click', function () {
                    // Hide filter panel.
                    $('.cd-filter-trigger, .cd-filter, .cd-tab-filter, .cd-gallery').each(function () {
                        $(this).removeClass('filter-is-visible');
                    });
                });

                // Filter work.
                // Filter bar.
                $('a.imoney-filter').click(function (e) {
                    e.preventDefault();
                    var mixType = $(this).data('mixtype');
                    var mixAnchor = $(this).data('mix');
                    mixController[mixType] = mixAnchor ? mixAnchor.split(' ') : [];
                    startFilter();
                });

                // Filter select.
                $('select.imoney-filter').change(function () {
                    var mixType = $(this).data('mixtype');
                    mixController[mixType] = $(this).val() ? $(this).val().split(' ') : [];
                    startFilter();
                });

                // Filter checkbox.
                $('input:checkbox.imoney-filter').change(function () {
                    var mixCheckbox = '';
                    var $currentCheckbox = $(this);
                    var mixType = $currentCheckbox.data('mixtype');
                    $('input:checkbox.imoney-filter[data-mixtype="' + mixType + '"]').each(function() {
                        mixCheckbox += $(this).is(':checked') ? ' ' + $(this).data('mix') : '';
                    });
                    mixController[mixType] = mixCheckbox ? mixCheckbox.slice(1).split(' ') : [];
                    startFilter();
                });

                // Filter checkbox.
                $('input:radio.imoney-filter').change(function () {
                    var mixType = $(this).data('mixtype');
                    var mix = $(this).data('mix');
                    if (mix && $(this).is(':checked')) {
                        mixController[mixType] =  mix.split(' ');
                    }
                    else {
                        mixController[mixType] = [];
                    }
                    startFilter();
                });

                var startFilter = function () {
                    /**
                     * Right string for complex and and or is like this example:
                     * .maybank.s-0.petrol,.maybank.s-1500.petrol
                     */
                    var filterArray = [];
                    for (var key in mixController) {
                        if (!mixController.hasOwnProperty(key)) continue;
                        var mix = mixController[key];
                        if (mix.length) {
                            if (!filterArray.length) {
                                $.each(mix, function (mixKey, itemMix) {
                                    filterArray.push([itemMix]);
                                });
                            }
                            else {
                                var tempArray = [];
                                $.each(filterArray, function(filterKey, filterItem) {
                                    $.each(mix, function (rawKey, rawItem) {
                                        // Clone filterItem instead of targetting it using operand '='.
                                        var tempFilter = filterItem.slice(0);
                                        tempFilter.push(rawItem);
                                        tempArray.push(tempFilter);
                                    });
                                });
                                filterArray = tempArray.slice(0);
                            }
                        }
                    }

                    // Process array of logics.
                    var filterStrings = [];
                    $.each(filterArray, function (keyArray, valueArray) {
                        var singleString = valueArray.join('.');
                        filterStrings.push('.' + singleString);
                    });

                    // Join arrays into string.
                    var filterString = 'all';
                    if (filterStrings.length) {
                        filterString = filterStrings.join(', ');
                    }

                    console.log(filterString);
                    $('#suggested-broadband').mixItUp('multiMix', {
                        filter: filterString
                    });
                };

                // Old code.
                //mobile version - detect click event on filters tab
                var filter_tab_placeholder = $('.cd-tab-filter .placeholder a'),
                    filter_tab_placeholder_default_value = $('.cd-tab-filter-wrapper').data('filter_label'),
                    filter_tab_placeholder_text = filter_tab_placeholder.text();

                $('.cd-tab-filter li').on('click', function (event) {
                    //detect which tab filter item was selected
                    var selected_filter = $(event.target).data('type');

                    //check if user has clicked the placeholder item
                    if ($(event.target).is(filter_tab_placeholder)) {
                        (filter_tab_placeholder_default_value == filter_tab_placeholder.text()) ? filter_tab_placeholder.text(filter_tab_placeholder_text) : filter_tab_placeholder.text(filter_tab_placeholder_default_value);
                        $('.cd-tab-filter').toggleClass('is-open');

                        //check if user has clicked a filter already selected
                    } else if (filter_tab_placeholder.data('type') == selected_filter) {
                        filter_tab_placeholder.text($(event.target).text());
                        $('.cd-tab-filter').removeClass('is-open');

                    } else {
                        //close the dropdown and change placeholder text/data-type value
                        $('.cd-tab-filter').removeClass('is-open');
                        filter_tab_placeholder.text($(event.target).text()).data('type', selected_filter);
                        filter_tab_placeholder_text = $(event.target).text();

                        //add class selected to the selected filter item
                        $('.cd-tab-filter .selected').removeClass('selected');
                        $(event.target).addClass('selected');
                    }
                });
            }
        }
    });

    //***********************************
    //  Show / hide feature.
    //***********************************
    $('.promo-hidden-features').on('click', function (e) {
        e.preventDefault();
        $button = $(this);
        $button.toggleClass('active').parents('.promo-gift').next('.item__details--cc').stop().slideToggle('fast', function() {
            if ($button.hasClass('active')) {
                $button.text($button.data('hide'));
            }
            else {
                $button.text($button.data('show'));
            }
        });
    });

    //***********************************
    //  FAQ / Ask Us tabs
    //***********************************
    // @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/tab.js";
    // @codekit-prepend "../../bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";
    $('#myTab').find('a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    //Handle featured box click
    $('#suggested-broadband > li').on('click', function(e) {
        // e.preventDefault();  Not needed to allow <a></a> tag to function
        var productUrl = $(this).data('url');

        if( e.target.nodeName.toLowerCase() !== 'a' && productUrl){
            window.location = $(this).data('url');
        }
        // if(e.target.nodeName.toLowerCase() !== 'a') {
        //     $(this).find('.btn-apply').click();
        // }
    });
});
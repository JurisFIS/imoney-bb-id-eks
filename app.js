'use strict';

// For NewRelic
require('dotenv').config();
if (process.env.NEWRELIC) global.newrelic = require('newrelic');

// Load modules.
const express = require('express'),
    path = require('path'),
    swig = global.swig = require('swig'),
    minifyHTML = require('express-minify-html'),
    compression = require('compression');

//Call GC forcefully
if (global.gc) {
  setInterval(() => {
    global.gc();
    console.log('GC done');
  }, 1000 * 30);
}

// Application up and running.
const app = global.app = express();

/*********************************************************
 ** Check configuration
 **********************************************************/
//Loan and Check Konfig
try {
  const config = global.config = require('./config');

  //Load newrelic when config loaded
  require('./custom_modules/iMoneyNewrelic')(app);

  global.debugName = config.name;
}
catch (e) {
  throw e;
}

// Custom libraries.
global.nodeEnv = process.env.NODE_ENV;
const env = global.env = app.get('env');
global.languages = require(
    './bower_components/imoney-assets/custom_modules/libraries/languages');
global.format = require(
    './bower_components/imoney-assets/custom_modules/libraries/format');
global.iCollection = require(
    './bower_components/imoney-assets/custom_modules/iCollection');
const Metatag = global.Metatag = require(
    './bower_components/imoney-assets/data_models/metatag');

// Debug.
const debugName = global.debugName;
const debug = require('debug')(`${debugName}:App`),
    error = require('debug')(`${debugName}:App:error`);

/**
 * Cache manager
 */
const iCacheManager = require('./custom_modules/iMoneyCacheManager');
app.use('/cache', iCacheManager.router);
app.use(iCacheManager.cacheManager);

//Lib iServer connectivity
const isitemap = require('./custom_modules/iclient/isitemap'),
    iclient = require('./custom_modules/iclient/iclient')(config.imaster),
    isystem = require('./custom_modules/iclient/isystem')(config);

//Sync system health info
isystem.on('update', function(sysinfo) {
  iclient.setConfig('system', sysinfo);
});

//Sync sitemap
isitemap.on('change', function(sitemap) {
  iclient.setConfig('sitemap', sitemap);
});

// Application extensions.
app.use(compression());
app.locals.pretty = true;
app.use(isitemap.sitemap());
app.use(express.static(path.join(__dirname, 'public')));

// Application configuration.
app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

//by default template caching must be disabled
swig.setDefaults({cache: false});

app.use((req, res, next) => {
  const test = /\?[^]*\//.test(req.url);
  var url = req.url.split("?")
  if (url[0].substr(-1) === "/" && req.url.length > 1 && !test) {
    res.redirect(301, url[1] ? url[0].slice(0, -1) + "?" + url[1] : req.url.slice(0, -1));
  } else next();
});

// Application routes.
app.use('/', (req, res, next) => {
  Metatag.update();
  next();
}, require('./routes/index'));

// production error handler
// no stacktraces leaked to user
app.use((req, res) => {
  res.status(404);
  res.render('../bower_components/imoney-assets/dev/templates/404', {
    bodyClass: 'imoney-' + config.region,
    region: config.region,
    pageTitle: "404 | iMoney",
    uri: req.originalUrl,
    lang: config.lang,
    app: config,
  });
});

module.exports = app;

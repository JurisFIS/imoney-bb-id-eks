function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split('&');
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    if (decodeURIComponent(pair[0]) == variable) {
      return decodeURIComponent(pair[1]);
    }
  }
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}
var uInitData = {};
function setUserData(uData) {
    uInitData = uData;
}

function getUserData()
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    var userData = {};

    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');

        if(!userData[sParameterName[0]]) userData[sParameterName[0]] = [];
        userData[sParameterName[0]].push(sParameterName[1]);
    }

    _.each(userData, function(item, index) {
      userData[index] = userData[index].join(',');
      userData[index] = unescape(userData[index]);
      userData[index] = userData[index].replace(/\+/g, ' ');
    });

    if(typeof userData['salary'] != 'undefined') {
        userData['min_salary'] = (userData['salary'] + '').replace(/(.*)RM/, '').split('-')[0];
    }

    return _.extend(userData,uInitData);
}

var cookie_utm_src = typeof utmcsr != "undefined" ? utmcsr : '';
var cookie_utm_medium = typeof utmcmd != "undefined" ? utmcmd : '';
var cookie_utm_campaign = typeof utmccn != "undefined" ? utmccn : '';

var utm_source = getQueryVariable('utm_source') ? getQueryVariable('utm_source'): cookie_utm_src;
var utm_medium = getQueryVariable('utm_medium') ? getQueryVariable('utm_medium'): cookie_utm_medium;
var utm_term = getQueryVariable('utm_term') ? getQueryVariable('utm_term'): '';
var utm_content = getQueryVariable('utm_content') ? getQueryVariable('utm_content'): '';
var utm_campaign = getQueryVariable('utm_campaign') ? getQueryVariable('utm_campaign'): cookie_utm_campaign;
var src_region = getQueryVariable('src') ? getQueryVariable('src'): '';

var JAPP = {
    config: {
        servers: {
            get_default: function() {
                if('offers.imoney.my' == document.location.hostname) 
                    return this.prod;

                if('https:' == document.location.protocol) 
                    return this.prod;

                return this.dev;
            },
            dev: {
                api_key: 'b43fd3fb4d6ffc1b3470593e3e6a7b16-us5',
                url: 'http://dev.formapp.ly/api/save/submission',
                campaigns: {
                    smart_search_2: '54acdeebdaaf453e7b8b4568',
                    smart_search_2a: '550a42fd26d8a53a728b4567',
                    smart_search_2b: '550a43ab26d8a5f3728b4567',
                    smart_search_4: '550a42fd26d8a53a728b4567',
                    smart_search_10: '55b8a42bdaaf458e438b456c',
                    smart_search_groupon: '551a16f626d8a5f06b8b4567',
                    call_me_back: '5493767ca57d69714e8b4567',
                    call_me_back_homepage: '552f2fc1daaf45b8158b4568',
                    smart_search_2_bb: '54c0b26edaaf459d6b8b456a',
                    call_me_back_bb: '54c1b99ddaaf45894b8b4567' ,
                    call_me_broadband: '54c1b99ddaaf45894b8b4567',
                    Ken_Trade: '5521dee4a57d69f1348b4567',
                    Standard_FA: '5523855ba57d69156e8b4567',
                    AIA_iMoney_Articles: '55238639a57d6962198b4567',
                    smart_search_aeon: '55404d81daaf45a9688b457a',
                    smart_search_publicbank: '55404e2fdaaf458f0f8b4567',
                    smart_search_income: '55404ed7daaf4514648b4576',
                    Maybank_Lazada_Promo: '555e9253daaf453b788b456f',
                    Maybank_Groupon_Promo: '5577fa47daaf454a118b456d',
                    Zalora_Promo: '5580f948daaf45bd258b456f'
                }
            },
            prod: {
                api_key: 'b43fd3fb4d6ffc1b3470593e3e6a7b16-us5',
                url: 'https://apply.imoney.my/api/save/submission',
                campaigns: {
                    smart_search_2: '54acdeebdaaf453e7b8b4568',
                    smart_search_2a: '550a42fd26d8a53a728b4567',
                    smart_search_2b: '550a43ab26d8a5f3728b4567',
                    smart_search_4: '550a42fd26d8a53a728b4567',
                    smart_search_10: '55b8a39326d8a5e6778b4567',
                    smart_search_groupon: '551a16f626d8a5f06b8b4567',
                    call_me_back: '5493767ca57d69714e8b4567',
                    call_me_back_homepage: '552e431d26d8a55d2d8b4569',
                    smart_search_2_bb: '54c0b26edaaf459d6b8b456a',
                    call_me_back_bb: '54c1b99ddaaf45894b8b4567',
                    call_me_broadband: '54c1b99ddaaf45894b8b4567',
                    Ken_Trade: '5521dee4a57d69f1348b4567',
                    Standard_FA: '5523855ba57d69156e8b4567',
                    AIA_iMoney_Articles: '55238639a57d6962198b4567',
                    smart_search_aeon: '55378df0a57d69d02d8b4567',
                    smart_search_publicbank: '5537ceeecbb648ae3d8b4567',
                    smart_search_income: '5538ce85cbb648516f8b4569',
                    Maybank_Lazada_Promo: '5562ee43a57d6964408b4567',
                    Maybank_Groupon_Promo: '5577e98638a5a5e4618b456c',
                    Zalora_Promo: '5581000038a5a5ee138b4567'
                }
            }
        }
    },
    createSubmissionWithProduct: function(product, callback, callback_params) {

        var params = getUserData();
        params['campaign_id'] = this.config.servers.get_default().campaigns.smart_search_2;
        params['product_name'] = product.name;
        params['referrer_submission_id'] = params.referrer;
        params['nid'] = product.nid;
        submitToJapp(params, callback, callback_params);

        return false;
    },
    createSubmissionWithProductBroadband: function(product, callback, callback_params) {
        var params = getUserData();
        params['campaign_id'] = this.config.servers.get_default().campaigns.smart_search_2_bb;
        params['product_name'] = product.name;
        params['referrer_submission_id'] = params.referrer;
        params['nid'] = product.nid;
        submitToJapp(params, callback, callback_params);

        return false;
    }
};

function submitPreferToTalk(form, callback, campaignId) {
    campaignId = typeof campaignId == 'undefined' ? JAPP.config.servers.get_default().campaigns.call_me_back : campaignId;

    var params = {
        'campaign_id': campaignId,
        'NAME': $(form).find("input[name='name']").val(),
        'EMAIL': $(form).find("input[name='email_address']").val(),
        'PHONE': $(form).find("input[name='phone_number']").val()
    };

    submitToJapp(params, function(error, eventName, params) {
        if(eventName == 'beforeSend') {
            $(form).find('button').html('Saving...').attr('disabled', 'disabled');
        }
        if(eventName == 'complete') {
            $(form).find('button').html('Done!');
            if(typeof callback == 'function') callback(null, form, 'done');
        }
    });

    return false;
}


function submitPreferToTalkbb(form, callback) {
    var params = {
        'campaign_id': JAPP.config.servers.get_default().campaigns.call_me_broadband,
        'NAME': $(form).find("input[name='name']").val(),
        'EMAIL': $(form).find("input[name='email_address']").val(),
        'PHONE': $(form).find("input[name='phone_number']").val()
    };

    submitToJapp(params, function(error, eventName, params) {
        if(eventName == 'beforeSend') {
            $(form).find('button').html('Saving...').attr('disabled', 'disabled');
        }
        if(eventName == 'complete') {
            $(form).find('button').html('Done!');
            if(typeof callback == 'function') callback(null, form, 'done');
        }
    });

    return false;
}



function submitToJapp(params, callback, callback_params) {
    if(!params) return;
    
    var seoData = {
        'UTSOURCE'  : userManager.utms.utmcsr,  //CS: Campaign source
        'UTMEDIUM'  : userManager.utms.utmcmd,  //MD: Medium
        'UTTERM'    : userManager.utms.utmctr,  //TR: Campaign term
        'UTCAMPAIGN': userManager.utms.utmccn,  //CN: Campaign name
        'UTCONTENT' : utm_content,
        'SRCREGION' : src_region
    }

    params = _.defaults(params, seoData);
    params.imuTrackId   = userManager.data.trackId;
    params.imuUserId    = userManager.data.uId;

    var data = {
        apikey: JAPP.config.servers.get_default().api_key,
        merge_vars: params
    }

    jQuery.ajax({
        async: false,
        type: "POST",
        url: JAPP.config.servers.get_default().url,
        data: data,
        dataType: 'jsonp',
        beforeSend: function () {
            if(typeof callback == 'function') callback(null, 'beforeSend', params, callback_params);
        },
        success:function(data) {
            var myValue = data.data.$id;
            $("#referrer").attr("value",myValue);
        }
    }).complete(function () {
        if(typeof callback == 'function') callback(null, 'complete', params, callback_params);
    });
}
'use strict';

const
    express = require('express'),
    request = require('request'),
    router = express.Router(),
    isitemap = require('../custom_modules/iclient/isitemap'),
    languages = global.languages,
    Broadband = require('../custom_modules/data_models/broadbandData'),
    Provider = require('../custom_modules/data_models/providerData'),
    Metatag = global.Metatag,
    config = global.config,
    debugName = global.debugName,
    debug = require('debug')(`${debugName}:router`),
    lang_prefix = config['lang_prefix'] ? `${config['lang_prefix']}/` : '',
    base = `/${lang_prefix}`,
    swig = global.swig;

// Add speed filter into swig.
swig.setFilter('speed', speed => speed < 1000 ? languages.translate('@var Mbps',
    '_products.html',
    {'@var': speed}) : languages.translate('@var Gbps', '_products.html',
    {'@var': (speed / 1000)}));

Broadband.on('add', registerRouteFor);

Broadband.on('remove', product => {
  debug('Removing : ' + product.get('path_alias'));
  registerRouteFor(product);
});

function registerRouteFor(product) {
  router.get(isitemap.add(base + product.get('path_alias')).lastAdded(),
      (req, res, next) => {
        if (req.url.endsWith("/")) {
          res.redirect(301, req.url.replace(/\/$/, ""));
        }
        removeHandler(product, Broadband, req, res, next);
      }, (req, res) => {
        Broadband.update();
        broadbandHandler(product, req, res);
      });
}

function broadbandHandler(product, req, res) {
  if (req.url.endsWith("/")) {
    res.redirect(301, req.url.replace(/\/$/, ""));
  }
  config.currentTemplate = 'product.html';
  res.render('../views/product', {
    item: product,
    cheaper: Broadband.findWhere({nid: product.get('cheaperProduct')}),
    faster: Broadband.filter(
        item => (product.get('fasterProducts') || []).includes(
            item.get('nid'))),
    providerItem: Provider.findWhere({field_telco: product.get('field_telco')}),
    bodyClass: `imoney-${config.region}`,
    base: base,
    region: config.region,
    lang: config.lang,
    app: config,
  });
}

function removeHandler(product, collection, req, res, next) {
  if (collection.findWhere({nid: product.get('nid')})) next();
  else {
    res.status(404);
    res.render('../bower_components/imoney-assets/dev/templates/404', {
      bodyClass: `imoney-${config.region}`,
      region: config.region,
      country: config.country,
      uri: req.originalUrl,
      lang: config.lang,
    });
  }
}

Provider.on('add', registerRouteProvider);

Provider.on('remove', provider => {
  debug('removing : ' + provider.get('path_alias'));
  registerRouteProvider(provider);
});

function registerRouteProvider(provider) {
  // Product pages.
  router.get(isitemap.add(base + config.urls['bb-listing'] + '/' +
      provider.get('machine_name')).lastAdded(), (req, res, next) => {
    removeHandler(provider, Provider, req, res, next);
  }, (req, res) => {
    Provider.update();
    Broadband.update();
    providerHandler(provider, req, res);
  });
}

function providerHandler(provider, req, res) {
    if (req.url.endsWith("/")) {
      res.redirect(301, req.url.replace(/\/$/, ""));
    }
  // Product in providers.
  config.currentTemplate = 'provider.html';
  res.render('../views/provider', {
    item: provider,
    products: Broadband.where({field_telco: provider.get('field_telco')}),
    path_alias: provider.get('machine_name'),
    providerList: Provider,
    base: base,
    fbAppId: config.fbAppId,
    bodyClass: `imoney-${config.region}` + ' page-bb-provider',
    region: config.region,
    app: config,
    lang: config.lang,
  });
}

/**
 * Listing page
 */
router.get(isitemap.add(base + config.urls['bb-listing']).lastAdded(),
    (req, res) => {
      if (req.url.endsWith("/")) {
        res.redirect(301, req.url.replace(/\/$/, ""));
      }
      Broadband.update();
      Provider.update();
      config.currentTemplate = 'list.html';
      res.render('../views/list.html', {
        bodyClass: 'page-bb-listing imoney-' + config.region,
        broadbandData: Broadband,
        base: base,
        metadata: Metatag.get(lang_prefix + config.urls['bb-listing']),
        region: config.region,
        lang: config.lang,
        path_alias: config.urls['bb-listing'],
        app: config,
      });
    });

router.get('/', function (req, res) {
  res.send('GET request to the homepage');
});

/* provider page */
router.get(isitemap.add(base + config.urls['bb-thankyou']).lastAdded(),
    function(req, res) {
      Broadband.update();
      config.currentTemplate = 'thank-you.html';
      res.render('../views/thank-you.html', {
        bodyClass: 'page-bb-provider imoney-' + config.region,
        broadbandData: Broadband,
        base: base,
        region: config.region,
        lang: config.lang,
        app: config,
        metadata: Metatag.get(lang_prefix + config.urls['bb-thankyou']),
      });
    });

/**
 * SmartSearch page
 */

router.get(isitemap.add(base + config.urls['bb-smartsearch']).lastAdded(),
    function(req, res) {
      config.currentTemplate = 'smartsearch.html';
      res.render('../views/smartsearch.html', {
        bodyClass: `imoney-${config.region}`,
        base: base,
        config: config,
        region: config.region,
        lang: config.lang,
        app: config,
        metadata: Metatag.get(
            lang_prefix + config.urls['bb-smartsearch']),
      });
    });

/**
 * SmartSearch result page
 */
router.get(isitemap.add(base + config.urls['bb-results']).lastAdded(),
    function(req, res) {
      request({
        url: config.url + '/profile/getBroadbandSmartSearch/' + config.region +
            '/' + config.lang + req['_parsedOriginalUrl'].search,
        json: true,
      }, function(error, response, json_data) {
        if (!error && response.statusCode === 200) {
          config.currentTemplate = 'results.html';
          res.render('../views/results.html', {
            bodyClass: `imoney-${config.region}`,
            region: config.region,
            path_alias: 'results',
            query: req.query,
            lang: config.lang,
            app: config,
            base: base,
            broadband: json_data.broadband,
            metadata: Metatag.get(
                lang_prefix + config.urls['bb-results']),
          });
        }
        else {
          console.log('Smartsearch loaded fail in URL: ' + config.url +
              '/profile/getBroadbandSmartSearch/' + config.region + '/' +
              config.lang + req['_parsedOriginalUrl'].search);
        }
      });
    });

/**
 * Test new smartsearch, only on Malaysia.
 */
if (config.region === 'my' && config.lang === 'en') {
  /**
   * SmartSearch page
   */
  router.get(isitemap.add('/broadband/search-v2').lastAdded(),
      (req, res) => {
        config.currentTemplate = 'smartsearch-v2.html';
        res.render('../views/smartsearch-v2.html', {
          bodyClass: `imoney-${config.region}`,
          base: base,
          config: config,
          region: config.region,
          lang: config.lang,
          app: config,
          metadata: Metatag.get(lang_prefix + config.urls['bb-smartsearch']),
        });
      });

  /**
   * SmartSearch result page
   */
  // @TODO: Make it async & await.
  router.get(isitemap.add('/broadband/search-v2/results').lastAdded(),
      (req, res) => {
        request({
          url: config.url + '/profile/getBroadbandSmartSearch/' +
              config.region + '/' + config.lang +
              req['_parsedOriginalUrl'].search,
          json: true,
        }, (error, response, json_data) => {
          if (!error && response.statusCode === 200) {
            config.currentTemplate = 'results-v2.html';
            res.render('../views/results-v2.html', {
              bodyClass: `imoney-${config.region}`,
              region: config.region,
              query: req.query,
              lang: config.lang,
              app: config,
              base: base,
              broadband: json_data.broadband,
              metadata: Metatag.get(lang_prefix + config.urls['bb-results']),
            });
          }
          else {
            console.log('Smartsearch loaded fail in URL: ' + config.url +
                '/profile/getBroadbandSmartSearch/' + config.region + '/' +
                config.lang + req['_parsedOriginalUrl'].search);
          }
        });
      });
}

/**
 * AMP page for testing.
 */
if (config.region === 'sg') {
  router.get(isitemap.add('/broadband/myrepublic/amp').lastAdded(),
      (req, res) => {
        res.render('../views/myrepublic_amp.html');
      });
}

/**
 * Glossary page
 */
if (config.region === 'my') {
  router.get(isitemap.add(base + config.urls['bb-glossary']).lastAdded(),
      (req, res) => {
        config.currentTemplate = 'glossary.html';
        res.render('../views/glossary.html', {
          bodyClass: `imoney-${config.region}`,
          base: base,
          config: config,
          region: config.region,
          lang: config.lang,
          app: config,
          metadata: Metatag.get(lang_prefix + config.urls['bb-glossary']),
        });
      });
}

/**
 * Plan Recommendation Tool
 */
if (config.region === 'my') {
  router.get('/broadband/plan-recommendation', (req, res) => {
    Broadband.update();
    Provider.update();
    res.render('../views/plan-recommendation.html', {
      bodyClass: `imoney-${config.region}`,
      base: base,
      config: config,
      region: config.region,
      lang: config.lang,
      app: config,
      broadbands: Broadband,
      providers: Provider,
      metadata: Metatag.get('broadband/plan-recommendation'),
    });
  });

  router.get('/broadband/plan-recommendation/best', (req, res) => {
    Broadband.update();
    Provider.update();
    try {
      const speed = req.query.speed;
      const price = req.query.price;
      const state = req.query.state;

      // Fall to fail when either of the 3 params doesn't meet.
      if (!speed || !price || !state) {
        res.json({
          success: false,
          message: 'Not enough data',
        });
      }

      const result = {
        success: true,
        faster: false,
        cheaper: false,
      };
      const timeAvailable = ['kl', 'selangor', 'penang'];

      // Get better speeds, same price and available at state.
      const fasters = Broadband.filter(
          product => (
              product.get('field_monthly_fee_rm_') <= price &&
              product.get('field_speed') > speed &&
              product.get('field_telco') !== 'astro' &&
              (
                  timeAvailable.indexOf(state) !== -1 ||
                  product.get('field_telco') !== 'TIME'
              )
          ),
      ).sort((a, b) => (b.get('field_speed') - a.get('field_speed')));

      if (fasters.length) result.faster = fasters.shift();

      const cheapers = Broadband.filter(
          product => (product.get('field_monthly_fee_rm_') < price &&
              product.get('field_speed') >= speed &&
              !(product.get('field_telco') === 'astro') &&
              (timeAvailable.indexOf(state) !== -1 ||
                  product.get('field_telco') !== 'TIME'))).
          sort((a, b) => (a.get('field_monthly_fee_rm_') -
              b.get('field_monthly_fee_rm_')));

      if (cheapers.length) result.cheaper = cheapers.shift();
      res.json(result);
    }
    catch (e) {
      res.json({
        success: false,
        message: e,
      });
    }
  });
}

/**
 * For bypassing images.
 */
router.get('/sites/*', (req, res, next) => {
  try {
    res.redirect(`${config.url}${req.path}`);
  }
  catch (e) {
    next();
  }
});

/**
 * Cache manager
 */
var iCacheManager = require('../custom_modules/iMoneyCacheManager');
iCacheManager.eventEmitter.on('cacheClear', () => {
  Broadband.fetch();
  Provider.fetch();
  Metatag.fetch();
});

// Good for ignoring socket.io errors (used for crate tracking)
router.get('/socket.io', (req, res) => {
  res.status(101).send('Switching protocol');
});

module.exports = router;

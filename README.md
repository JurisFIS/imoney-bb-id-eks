# IMONEY 4 BROADBAND DOCUMENTATION

## Requirements:
* [iMoney 4 Assets](https://bitbucket.org/imoneytech/imoney-4-assets/src)
    
## Configuration:
* Latest branch: [master](https://bitbucket.org/imoneytech/imoney-4-broadband/src/master/)
* Configuration file: `config/app.json` (ignored from git) 
* Configuration samples:
    - [config/app.json.sample](https://bitbucket.org/imoneytech/imoney-4-broadband/src/master/config/app.json.sample)
* Country supported: any
* Environment supported: YES
    

## Setup:
* Prepare configuration: Copy from configuration samples, or create new `config/app.json` file with suitable configurations.
* Satisfy requirements.
* Run setup command:
 ```bash
 npm install
 ```

## Run:
* Including environment variables:
    - `NODE_ENV`: **environment** (`my`|`ms`|`id`|`ph`|`sg`)
    - `PORT`: custom port that application listens to    
        Default: `3000`
* Run application:
    ```bash
    NODE_ENV=<env> PORT=<port> node bin/www
    ```
## Application components
## Application logic
* Provide broadband pages for website, including:
    * Listing page
    * Provider pages
    * Product pages
    * Search page
    * Search result page
    * Thank you page
    * Checking coverage page
    
### Templates
* Using `swig` as template engine
* Are stored in `views` folder

### Assets (CSS/JS/Images/Fonts)
* Development version of CSS/JS are stored in `dev`
* Production version is generated using Gulp and stored in `public`.

### Custom modules
* Containing custom module to control the application
* Containing data model to handle micro-cache for `Broadband product list` and `Broadband provider list`

## Routing logic:
* Static routes are dynamically registered base on configurations in `app.json` to make sure it's translatable. An example
```json
{
    "urls": {
          "article-feed": "articles/feed",
          "category-feed": "articles/category/cards/feed",
          "bb-listing": "jalur-lebar",
          "bb-smartsearch": "jalur-lebar/cari",
          "bb-thankyou": "jalur-lebar/terima-kasih",
          "bb-results": "jalur-lebar/cari/hasil",
          "bb-coverage": "jalur-lebar/semak-liputan"
    }
}
```
* Dynamical routes are also dynamically registered and removed base on Backbone Collection.

## Application logic:
* When application starts, it registers all dynamical routes and start querying data for collections.
* Data source are configured in `app.json`. Example:
    ```json
    {
        "dataUrls": {
              "broadband": "https://direct.imoney.my/profile/getBroadband/my/en",
              "provider": "https://direct.imoney.my/profile/getBroadbandProvider/my/en",
              "metatag": "https://direct.imoney.my/v3/metatag"
        }
    }
    ``` 
* Queried data is controlled by instances of iCollection that is stored in `custom_modules\data_models`
* For every adding and removing event from Backbon Collection, a route is dynamically registered or removed.

## Test coverage:
* 0%

## Technical debts:
No
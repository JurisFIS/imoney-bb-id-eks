const express = require('express');
const app = express();
const port = 9999;

app.get('*', (req, res) => {
  try {
    let jsonData = require(`./.dump${req.path}.json`);
    res.json(jsonData);
  }
  catch (e) {
    res.status(500).send('No requested data');
  }
});

const server = app.listen(port, () => {
  console.log(`Test data server is running on port ${port}!`);
});

exports = module.exports = server;
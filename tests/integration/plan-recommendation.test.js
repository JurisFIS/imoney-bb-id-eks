// Test running page.

const request = require('supertest');
const cheerio = require('cheerio');

beforeAll(done => {
  process.env.NODE_ENV = 'test';
  global.appServer = require('../../bin/www');
  global.dataServer = require('../dataServer');

  // Wait for data to load.
  setTimeout(done, 100);
});
describe('UI page test', () => {
  let $, response, body;

  beforeEach(async done => {
    response = await request(global.appServer).
        get('/broadband/plan-recommendation');

    body = await response.text;
    $ = await cheerio.load(body);
    done();
  });

  test('Should use route /broadband/plan-recommendation', () => {
    expect(response.statusCode).toBe(200);
  });

  test('Should have metatag data', () => {
    expect($('title').text()).toBe('Test recommendation metatag title');
    expect($('meta[name="description"]').attr('content')).
        toBe('Test recommendation metatag description');
  });
});

describe('API page test', () => {
  beforeAll(done => {
    // Turn off update feature for Broadband product.
    let Broadband = require('../../custom_modules/data_models/broadbandData');

    // Mock Broadband update function.
    Broadband.update = jest.fn(callback => {
      callback();
    });

    request(global.dataServer).get('/broadband.recommendation').then(response => {
      const data = JSON.parse(response.text);
      Broadband.set(data.broadband);
      done();
    });
  });

  test('Should use route /broadband/plan-recommendation/best', done => {
    request(global.appServer).
        get('/broadband/plan-recommendation/best').
        expect('Content-Type', /json/).
        expect(200).end(response => {
          done();
    });
  });

  test('Should fail if no speed', async done => {
    const response = await request(global.appServer).
        get('/broadband/plan-recommendation/best?price=200&state=kl');
    const content = await JSON.parse(response.text);
    expect(content.success).not.toBeTruthy();
    done();
  });

  test('Should fail if no price', async done => {
    const response = await request(global.appServer).
        get('/broadband/plan-recommendation/best?speed=300&state=kl');
    const content = await JSON.parse(response.text);
    expect(content.success).not.toBeTruthy();
    done();
  });

  test('Should fail if no area', async done => {
    const response = await request(global.appServer).
        get('/broadband/plan-recommendation/best?price=200&speed=300');
    const content = await JSON.parse(response.text);
    expect(content.success).not.toBeTruthy();
    done();
  });

  test('Should not fail if enough data', async done => {
    const response = await request(global.appServer).
        get('/broadband/plan-recommendation/best?price=0&speed=0&state=dump');
    const content = await JSON.parse(response.text);
    expect(content.success).toBeTruthy();
    done();
  });

  test('Should return fastest product within price', async done => {
    const response = await request(global.appServer).
        get('/broadband/plan-recommendation/best?price=199&speed=0&state=kl');
    const content = await JSON.parse(response.text);
    expect(content.faster.field_speed).toEqual(1000);
    done();
  });

  test('Should return cheapest product within speed', async done => {
    const response = await request(global.appServer).
        get('/broadband/plan-recommendation/best?price=10000&speed=30&state=kl');
    const content = await JSON.parse(response.text);
    expect(content.cheaper.field_monthly_fee_rm_).toEqual(89);
    done();
  });
});

afterAll(done => {
  global['dataServer'].close();
  global.appServer.close(done);
});
'use strict';

const
    EventEmitter = require("events").EventEmitter,
    ee = new EventEmitter(),
    express = require('express'),
    cacheRouter = express.Router(),
    url = require('url');


exports = module.exports = {
    cacheManager: function(req, res, next) {
        //For handling clear cache command from query param. e.g. http://url-here.com/path-to-resource?cache=clear
        if (req.query.cache == 'clear') {
            ee.emit("cacheClear", url.parse(req.url).pathname, req);
        }

        next();
    },
    router: cacheRouter.get('*', function(req, res) {
        //For handling clear cache command from uri. e.g. http://url-here.com/cache/path-to-resource
        res.json({
            status: 'iMoneyCacheManager:ClearCache: ' + ee.listeners('cacheClear').length + ' handler(s) notified!',
            url: req.url
        });

        ee.emit("cacheClear", url.parse(req.url).pathname, req);
    }),
    eventEmitter: ee
};

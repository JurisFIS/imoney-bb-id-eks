module.exports = function(app) {
  if (!global.newrelic) return;

  // Middleware for adding custom attributes
  // These map to environment variables exposed in the pod spec
  const CUSTOM_PARAMETERS = {
    'K8S_NODE_NAME': process.env.K8S_NODE_NAME,
    'K8S_HOST_IP': process.env.K8S_HOST_IP,
    'K8S_POD_NAME': process.env.K8S_POD_NAME,
    'K8S_POD_NAMESPACE': process.env.K8S_POD_NAMESPACE,
    'K8S_POD_IP': process.env.K8S_POD_IP,
    'K8S_POD_SERVICE_ACCOUNT': process.env.K8S_POD_SERVICE_ACCOUNT,
    'K8S_POD_TIER': process.env.K8S_POD_TIER
  };

  app.use(function(err, req, res, next) {
    global.newrelic.noticeError(err, CUSTOM_PARAMETERS);
  });
};
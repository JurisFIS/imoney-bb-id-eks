/*!
 * Sitemap for iMoney 4 applications
 * Copyright(c) 2015 iMoney
 */

'use strict';

/**
 * Module dependencies.
 * @private
 */
var parseUrl = require('parseurl');
var _ = require('underscore');
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var _sitemap = [];
var lastAdded = undefined;
var lastRemoved = undefined;
var defaultSitemapUri = '/sitemap.json';

/* Creates Sitemap function that extends from EventEmitter */
function Sitemap() {
    EventEmitter.call(this);
}
util.inherits(Sitemap, EventEmitter);

/**
 * Module exports.
 * @public
 */

exports = module.exports = _.extend(new Sitemap(), {
    add: add,
    getAll: getAll,
    lastAdded: getLastAdded,
    lastRemoved: getLastRemoved,
    sitemap: sitemap
});

/**
 * Serves the sitemap on the given `path`.
 *
 * @public
 * @param {String} path
 * @return {Function} middleware
 */

function sitemap(uri) {
    uri = uri || defaultSitemapUri;
    if (typeof uri !== 'string') throw new TypeError('uri for sitemap.json must be string');

    return function sitemap(req, res, next){
        if (parseUrl(req).pathname !== uri) {
            next();
            return;
        }

        if (req.method !== 'GET' && req.method !== 'HEAD') {
            res.statusCode = req.method === 'OPTIONS' ? 200 : 405;
            res.setHeader('Allow', 'GET, HEAD, OPTIONS');
            res.setHeader('Content-Length', '0');
            res.end();
            return;
        }

        res.json(_.unique(_sitemap).sort());
    };
};

/**
 * Adds uri to the sitemap
 *
 * @public
 * @param {String} uri
 * @return {Object} this
 */
function add(uri) {
    lastAdded = uri;
    if(_sitemap.indexOf(uri) !== -1) return this;

    _sitemap.push(uri);
    this.emit('add', uri);
    this.emit('change', _sitemap);

    return this;
}

/**
 * Removes uri to the sitemap
 *
 * @public
 * @param {String} uri
 * @return {Object} this
 */
function remove(uri) {
    lastRemoved = uri;
    if(_sitemap.indexOf(uri) === -1) return this;

    _sitemap = _.without(_sitemap, uri);
    this.emit('remove', uri);
    this.emit('change', _sitemap);

    return this;
}

/**
 * Returns the last uri handled by add() function
 *
 * @public
 * @return {String|undefined} lastAdded
 */
function getLastAdded() {
    return lastAdded;
}

/**
 * Returns the last uri handled by remove() function
 *
 * @public
 * @return {String|undefined} lastRemoved
 */
function getLastRemoved() {
    return lastRemoved;
}

/**
 * Returns the full sitemap
 *
 * @public
 * @return {Array} sitemap
 */
function getAll() {
    return _sitemap;
}

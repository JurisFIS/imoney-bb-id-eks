var os = require('os');
var _ = require('underscore');
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var networkInterfaces = os.networkInterfaces();
var IPs = getIPs();

/* Creates ISystem function that extends from EventEmitter */
function ISystem() {
    EventEmitter.call(this);
}
util.inherits(ISystem, EventEmitter);

var iSystem = new ISystem();

/**
 * Module exports.
 * @public
 */

exports = module.exports = function(konfig) {
    setInterval(function() {
        updateSysInfo(konfig);
    }, 5000);
    return iSystem;
};

function updateSysInfo(konfig) {
    iSystem.emit('update', {
        memory: getMem(),
        ips: IPs,
        port: process.env.PORT,
        uptime: process.uptime(),
        platform: process.platform,
        pid: process.pid,
        version: process.version,
        cwd: process.cwd(),
        konfig: konfig
    });
}

function getMem() {
    return process.memoryUsage();
}

function getIPs() {
    var addresses = [];
    for (var k in networkInterfaces) {
        for (var k2 in networkInterfaces[k]) {
            var address = networkInterfaces[k][k2];
            if (address.family === 'IPv4' && !address.internal) {
                addresses.push(address.address);
            }
        }
    }

    return addresses;
}
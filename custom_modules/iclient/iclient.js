/*!
 * Client for iMoney 4 applications
 * Copyright(c) 2015 iMoney
 */

'use strict';

/**
 * Module dependencies.
 * @private
 */
var _ = require('underscore');
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var iSystem = require('./isystem');
var socket = null;
var config = {type: 'iclient'};

var doSync = false;

/* Creates IClient function that extends from EventEmitter */
function IClinet() {
    EventEmitter.call(this);
}
util.inherits(IClinet, EventEmitter);
var iClient = _.extend(new IClinet(), {
    setConfig: setConfig,
    sync: sync
});

/**
 * Module exports.
 * @public
 */

exports = module.exports = function(url) {
    if (!url) throw new TypeError('uri for iServer is required');
    if (typeof url !== 'string') throw new TypeError('uri for sitemap.json must be string with format http://IP:PORT');

    socket = require('socket.io-client')(url);
    socket.on('connect', connect);
    socket.on('disconnect', disconnect);
    socket.on('reconnect', reconnect);

    return iClient;
};

/**
 * Set configuration to be synced with iServer
 *
 * @public
 * @param {String} namespace
 * @param {Object} conf
 */
function setConfig(namespace, conf) {
    config[namespace] = conf;
    sync();
}

/**
 * Triggers connect event
 */
function connect() {
    iClient.emit('connect');
}

/**
 * Triggers disconnect event
 */
function disconnect() {
    iClient.emit('disconnect');
}

/**
 * Triggers reconnect event
 */
function reconnect() {
    iClient.emit('reconnect');
}

/**
 * Triggers sync with iServer
 */
function sync() {
    doSync = true;
}

setInterval(function() {
    if(!doSync) return;

    socket.emit('clients:update', config);
    doSync = false;
}, 30000);
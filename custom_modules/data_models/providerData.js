'use strict';
const iCollection = global.iCollection,
    nodeEnv = global.nodeEnv,
    providerCollection = new iCollection(
        'provider',
        global.config['dataUrls'].provider,
        'providers',
    );

exports = module.exports = providerCollection.productCollection;
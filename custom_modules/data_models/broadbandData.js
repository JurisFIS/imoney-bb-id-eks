'use strict';
const iCollection = global.iCollection,
    nodeEnv = global.nodeEnv,
    bbCollection = new iCollection(
        'broadband',
        global.config['dataUrls'].broadband,
        'broadband',
    );

exports = module.exports = bbCollection.productCollection;
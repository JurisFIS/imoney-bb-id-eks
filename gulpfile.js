// include gulp
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    // jshint = require('gulp-jshint'),
    stripDebug = require('gulp-strip-debug'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin'),
    // babel = require('gulp-babel'),
    // log = require('gulplog'),
    // browserify = require('browserify'),
    // tap = require('gulp-tap'),
    // buffer = require('gulp-buffer'),
    // sourcemaps = require('gulp-sourcemaps'),
    // watch = require('gulp-watch'),
    uglify = require('gulp-uglify');


    gap = require('gulp-append-prepend');

const COMPONENT_SOURCE = './bower_components/imoney-assets/bower_components/',
    BOOTSTRAP_JS = 'bootstrap-sass/assets/javascripts/bootstrap/';

/**
 * Javascript.
 */

/**
 * @param customConfig
 */
function gulp_javascript(customConfig) {
  let config = {
    required: [],
    destination: './public/bbrunner/js',
  };
  Object.assign(config, customConfig);
  config.output = config.output ||
      config.watch.replace(/^(.*)\/(.+)\.js$/, '$2.min.js');
  let list = [...config.required, ...[config.watch]];
  return gulp.src(list).
      pipe(concat(config.output)).
      pipe(stripDebug()).
      pipe(uglify()).
      pipe(gulp.dest(config.destination));
}

gulp.task('search', () => {
  gulp_javascript({
    required: [
      COMPONENT_SOURCE + '/slick.js/slick/slick.min.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'modal.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'transition.js',
    ],
    watch: './dev/js/bb-smartsearch.js',
  });
});

gulp.task('results', () => {
  gulp_javascript({
    required: [
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'modal.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'transition.js',
    ],
    watch: './dev/js/bb-results.js',
  });
});

gulp.task('search-v2', () => {
  gulp_javascript({
    required: [
      COMPONENT_SOURCE + 'slick.js/slick/slick.min.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'modal.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'transition.js',
    ],
    watch: './dev/js/bb-smartsearch-v2.js',
  });
});

gulp.task('results-v2', () => {
  gulp_javascript({
    required: [
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'modal.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'transition.js',
    ],
    watch: './dev/js/bb-results-v2.js',
  });
});

gulp.task('product', () => {
  gulp_javascript({
    required: [
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'modal.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'transition.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'tab.js',
    ],
    watch: './dev/js/bb-product.js',
  });
});

gulp.task('listing', () => {
  gulp_javascript({
    required: [
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'modal.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'transition.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'tab.js',
    ],
    watch: './dev/js/bb-listing.js',
  });
});

gulp.task('provider', () => {
  gulp_javascript({
    required: [
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'modal.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'transition.js',
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'dropdown.js',
    ],
    watch: './dev/js/bb-provider.js',
  });
});

gulp.task('thankyou', () => {
  gulp_javascript({
    watch: './dev/js/bb-product-thank-you.js',
  });
});

gulp.task('recommendation', () => {
  gulp_javascript({
    required: [
      COMPONENT_SOURCE + BOOTSTRAP_JS + 'modal.js',
    ],
    watch: './dev/js/bb-recommendation.js',
  });
});

/**
 * CSS.
 */
gulp.task('sass-thankyou-fix', () => {
  gulp.src('./dev/scss/bb-product-thank-you.scss').
    pipe(sass().on('error', sass.logError)).
    pipe(cssmin()).
      pipe(gulp.dest('./public/bbrunner/css/'));
});

/**
 * CSS.
 */
gulp.task('sass-thankyou', () => {
  gulp.src('./dev/scss/bb-product-thank-you.scss').
      pipe(gap.prependFile('bower_components/imoney-assets/dev/scss/_inc/user-review-acquisition.scss'))
      .pipe(sass()).
    pipe(cssmin()).
      pipe(gulp.dest('./public/bbrunner/css/'));
});

gulp.task('sass-listing', () => {
  gulp.src('dev/scss/bb-listing.scss').
      pipe(sass({outputStyle: 'compressed'})).
      pipe(gulp.dest('./public/bbrunner/css'));
});

gulp.task('sass-provider', () => {
  gulp.src('dev/scss/bb-provider.scss').
      pipe(sass({outputStyle: 'compressed'})).
      pipe(gulp.dest('./public/bbrunner/css'));
});

gulp.task('sass-product', () => {
  gulp.src('dev/scss/bb-product.scss').
      pipe(sass({outputStyle: 'compressed'})).
      pipe(gulp.dest('./public/bbrunner/css'));
});

gulp.task('sass-ss', () => {
  gulp.src('dev/scss/bb-smartsearch.scss').
      pipe(sass({outputStyle: 'compressed'})).
      pipe(gulp.dest('./public/bbrunner/css'));
});

gulp.task('sass-ss-results', () => {
  gulp.src('dev/scss/bb-results.scss').
      pipe(sass({outputStyle: 'compressed'})).
      pipe(gulp.dest('./public/bbrunner/css'));
});